---
title: "Opensource softwares - Prolog"
description: "Prolog implementations"
featured_image: ''
menu: ragno
date: 2024-01-12
draft: false
tags:
- prolog
- opensource
- programming languages

---

- SWI-Prolog: [homepage](https://www.swi-prolog.org), [repo](https://github.com/SWI-Prolog/swipl-devel)
- GNU Prolog: [homepage](http://www.gprolog.org), [repo](https://github.com/didoudiaz/gprolog)
- YAP Prolog: [homepage](https://www.dcc.fc.up.pt/~vsc/yap), [repo](https://github.com/vscosta/yap)
- B-Prolog: [homepage](http://www.picat-lang.org/bprolog), [repo](https://github.com/rurban/picat-lang)
- Ciao Prolog: [homepage](https://ciao-lang.org), [repo](https://github.com/ciao-lang/ciao)
- Scryer Prolog: [homepage](https://github.com/mthom/scryer-prolog), [repo](https://github.com/mthom/scryer-prolog)
- XSB Prolog: [homepage](http://xsb.sourceforge.net), [repo](https://sourceforge.net/projects/xsb)
- ECLiPSe Prolog: [homepage](https://eclipseclp.org), [repo](https://github.com/eclipse-clp/eclipse-clp)
- Tau Prolog: [homepage](http://tau-prolog.org), [repo](https://github.com/tau-prolog/tau-prolog)
- Logtalk: [homepage](https://logtalk.org), [repo](https://github.com/LogtalkDotOrg/logtalk3)
- tuProlog: [homepage](http://tuprolog.unibo.it), [repo](https://github.com/tuProlog/2p-kt)
- Strawberry Prolog: [homepage](http://www.dobrev.com), [repo](https://github.com/DobrevDeveloper/StrawberryProlog)
- Lean Prolog: [homepage](https://github.com/rla/lean-prolog), [repo](https://github.com/rla/lean-prolog)
- Trealla: [homepage](https://github.com/trealla-prolog/trealla), [repo](https://github.com/trealla-prolog/trealla)
- N-Prolog: [homepage](https://github.com/sasagawa888/nprolog), [repo](https://github.com/sasagawa888/nprolog)
