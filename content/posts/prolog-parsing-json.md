---
title: 'Calling http rest API and parsing json response with (scryer) #Prolog'
date: 2024-03-02
draft: false
tags:
- prolog
- programming
---

In this simple code I'll show how you can call a rest api (qliksense saas rest api) and parse its json response 

``` prolog

?- use_module(library(dcgs)).
?- use_module(library(pairs)).
?- use_module(library(serialization/json)).
?- use_module(library(http/http_open)).

?- set_prolog_flag(answer_write_options, [max_depth(1000)]).

?- http_open("https://mytenant.eu.qlikcloud.com/api/v1/tenants/me", S, [request_headers(['Authorization'("Bearer MYKEY")])]), 
	get_n_chars(S, _N, HTML),
	phrase(json_chars(pairs(J)), HTML), 
	member(string("id")-string(ID), J).

 HTML = "{\"id\":\"myid\",\"hostnames\":[\"mytenant.eu.qlikcloud.com\"]}", J = [string("id")-string("myid"),string("hostnames")-list([string("mytenant.eu.qlikcloud.com)])], ID = "myid"
```
