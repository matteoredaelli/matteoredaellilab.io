---
title: 'Howto batch Install OBIEE 12c (silent mode)'
date: Thu, 24 Nov 2016 12:24:00 +0000
draft: false
tags: ['Me']
---

If you wanted to install / deploy automatically obiee systems in a datacenter/cloud you could simply run few simple commands like:```
export TEMP=/home/oracle/tmp
export TEMPDIR=/home/oracle/tmp
export JAVA\_HOME=/home/oracle/apps/jdk1.8.0

java -Djava.io.tmpdir=/home/oracle/tmp -jar fmw\_12.2.1.2.0\_infrastructure.jar \\
     -silent -responseFile /home/oracle/KIT/response\_01\_fmw\_infrastructure.rsp \\
     -invPtrLoc /home/oracle/oraInst.loc

./bi\_platform-12.2.1.2.0\_linux64.bin -silent \\
      -responseFile /home/oracle/KIT/response\_02\_bi\_platform.rsp \\
      -invPtrLoc /home/oracle/oraInst.loc \\
      -ignoreSysPrereqs

export ORACLE\_HOME=/home/oracle/Oracle/Middleware/Oracle\_Home
export BI\_PRODUCT\_HOME=$ORACLE\_HOME/bi
$BI\_PRODUCT\_HOME/bin/config.sh -silent \\
    -responseFile /home/oracle/KIT/response\_03\_bi\_platform\_config.rsp \\
    -invPtrLoc /home/oracle/oraInst.loc \\
    -ignoreSysPrereqs![](http://www.oracle.com/us/assets/cw20v1-bi-overview-2720811.jpg)
```
---
### Comments:
#### 
[Rahul]( "panchalrahul6@gmail.com") - <time datetime="2018-12-13 14:45:08">Dec 4, 2018</time>

from where to get those silent files ?
<hr />
