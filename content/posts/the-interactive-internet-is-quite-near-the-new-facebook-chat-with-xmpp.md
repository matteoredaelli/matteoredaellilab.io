---
title: 'The Interactive Internet is quite near... the new facebook chat with XMPP...'
date: Sat, 13 Feb 2010 15:20:22 +0000
draft: false
tags: ['chat', 'facebook', 'LinkedIn', 'Me', 'OpenSource', 'xmpp']
---

[![](http://www.redaelli.org/matteo-blog/wp-content/uploads/2010/02/aoqokvjc.png "aoqokvjc")](http://blog.facebook.com/blog.php?post=297991732130)After Google with GTalk (~ 180 million users) , now also Facebook [![](http://www.redaelli.org/matteo-blog/wp-content/uploads/2010/02/xmpp.png "xmpp")](http://www.xmpp.org)(> 400 million users ) has [switched](http://blog.facebook.com/blog.php?post=297991732130) its chat protocol to [Jabber](http://www.jabber.org/)/[XMPP](http://xmpp.org/) ( [pdf](http://www.saint-andre.com/jabber/Jabber-Real-Time-Internet.pdf) introduction). Now you need simply a [generic xmpp client](http://xmpp.org/software/clients.shtml) (like Adium, iChat, Miranda, [Pidgin](http://www.pidgin.im/)) and configure Facebook, Gtalk,.. XMPP is now the de facto the Standard protocol for Instant Messaging **We should not use anymore chat systems/operators that use proprietary standards like Skype, MS Messanger and Communicator, ..**[![](http://www.pidgin.im/shared/img/logo.pidgin.png "pidgin")](http://www.pidgin.im/) We are next to have an **Interactive Internet**: like emails that can be sent out of a domain, we'll be able to chat with people outside our domain: with my gmail account I'll be able to chat with facebook people !!!!!!!!!!!! The new facebook chat (_chat.facebook.com)_ [runs](http://www.redaelli.org/matteo-blog/archives/297) an OpenSource software witten in Erlang, [ejabberd](http://www.ejabberd.im/) (commercial support at [process-one.net](http://www.process-one.net/en/ejabberd/)). That will be also used by the next [moodle](http://moodle.org/) 2.0.[![](http://www.redaelli.org/matteo-blog/wp-content/uploads/2010/02/talk_logo.gif "talk_logo")](http://www.redaelli.org/matteo-blog/wp-content/uploads/2010/02/talk_logo.gif) See the video "[Set-up an Enterprise Instant Messaging server in 180 seconds](http://www.process-one.net/tutorials/ejabberd_setup_linux_en_viewlet_swf.html)"
---
### Comments:
#### 
[matteo](http://www.redaelli.org/ "matteo.redaelli@gmail.com") - <time datetime="2010-02-13 18:58:49">Feb 6, 2010</time>

Is Yahoo also experimenting XMPP? Look at http://www.process-one.net/en/blogs/article/after\_aol\_yahoo\_is\_also\_experimenting\_with\_xmpp
<hr />
