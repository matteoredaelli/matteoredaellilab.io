---
title: 'Set a Guinness World Record Enjoy a Better Web'
date: Fri, 13 Jun 2008 17:03:58 +0000
draft: false
tags: ['firefox', 'OpenSource']
---

[![](http://www.spreadfirefox.com/files/images/affiliates_banners/dday_badge_fox.png)](http://www.spreadfirefox.com/node&id=0&t=264)Pledge to get Firefox 3 during Download Day to set the Guinness World Record for Most Software Downloaded in 24 Hours!
