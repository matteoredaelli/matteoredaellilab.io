---
title: 'Aboliamo le province, più potere alle regioni!'
date: Sun, 10 Jul 2011 18:39:07 +0000
draft: false
tags: ['Me', 'Politica']
---

![](http://www.idvpiceno.it/wp-content/uploads/2011/06/Italia-per-regioni-e-provincie.gif)Riduciamo i costi della politica italiana abolendo le province e dando alle regioni le competenze che oggi spettano alle province!  Di cosa si occupano le province oltre che manutenere le strade provinciali?