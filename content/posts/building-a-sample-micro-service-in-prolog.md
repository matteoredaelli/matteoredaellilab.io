---
title: 'Building a sample (micro) service in Prolog'
date: Sun, 07 Sep 2014 19:41:26 +0000
draft: false
tags:
- prolog
- microservice
---

![](http://www.swi-prolog.org/icons/swipl.png) Sometimes ago I read that some components of [IBM Watson](http://www.ibm.com/smarterplanet/us/en/ibmwatson/) were [implemented in prolog](http://www.cs.nmsu.edu/ALP/wp-content/uploads/2011/03/PrologAndWatson1.pdf) . So I decided to look at it again after many years... I like [Prolog](http://en.wikipedia.org/wiki/Prolog), I studied [prolog](http://en.wikipedia.org/wiki/Prolog) at Computer Science University of Milan and for my thesis I wrote code in [Prolog](http://en.wikipedia.org/wiki/Prolog) (and Lisp). [proloGraph](https://github.com/matteoredaelli/proloGraph) is a simple example of howto exposing a [prolog](http://en.wikipedia.org/wiki/Prolog) graph database to other applications,  building a REST web service. I used [swi-prolog](http://www.swi-prolog.org/) and its [http library](http://www.swi-prolog.org/pldoc/package/http.html) Install the prolog language (I used the fantastic [Linux Debian](https://www.debian.org/) distribution) with```
apt-get install swi-prolog
```clone my git repository```
git clone https://github.com/matteoredaelli/proloGraph
cd proloGraph
```Run it with```
swipl -s webserver.pl -g 'server(8765).'
```Open the following url with your browser```
http://localhost:8765/vertex?name=user(matteo)
```and you will get:```
{
  "prev": \[ {"from":"user(gabriele)", "to":"user(matteo)", "rel":"follow"} \],
  "next": \[
    {"from":"user(matteo)", "to":"user(ele)", "rel":"follow"},
    {"from":"user(matteo)", "to":"user(gabriele)", "rel":"follow"},
    {"from":"user(matteo)", "to":"user(4)", "rel":"follow"},
    {"from":"user(matteo)", "to":"country(italy)", "rel":"lives"},
    {"from":"user(matteo)", "to":"hobby(running)", "rel":"likes"}
  \]
}
```
