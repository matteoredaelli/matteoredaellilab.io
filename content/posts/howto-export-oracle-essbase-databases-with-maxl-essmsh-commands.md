---
title: 'Howto export Oracle Essbase databases with MaxL / essmsh commands'
date: Fri, 10 Apr 2015 08:27:26 +0000
draft: false
tags: ['Me']
---

Manually```
essbase@olap-server:~> /opt/essbase/Oracle/Middleware/EPMSystem11R1/products/Essbase/EssbaseServer/templates/startMaxl.sh

 Essbase MaxL Shell 64-bit - Release 11.1.2 (ESB11.1.2.3.505B018)
 Copyright (c) 2000, 2014, Oracle and/or its affiliates.
 All rights reserved.

MAXL> login Hypadmin mypassword on olap-server.redaelli.org;

 OK/INFO - 1051034 - Logging in user \[Hypadmin@Native Directory\].
 OK/INFO - 1241001 - Logged in to Essbase.

MAXL> export database P\_BSO.Plan1 level0 data to data\_file 'ExpLev0\_P\_BSO.Plan1';

 OK/INFO - 1054014 - Database Plan1 loaded.
 OK/INFO - 1051061 - Application P\_BSO loaded - connection established.
 OK/INFO - 1054027 - Application \[P\_BSO\] started with process id \[60396\].
 OK/INFO - 1019020 - Writing Free Space Information For Database \[Plan1\].
 OK/INFO - 1005031 - Parallel export completed for this export thread. Blocks Exported: \[2013908\]. Elapsed time: \[312.35\]..
 OK/INFO - 1005002 - Ascii Backup Completed. Total blocks: \[2.01391e+06\]. Elapsed time: \[312.35\]..
 OK/INFO - 1013270 - Database export completed \['P\_BSO'.'Plan1'\].


```Automatically```
/opt/essbase/Oracle/Middleware/EPMSystem11R1/products/Essbase/EssbaseServer/templates/startMaxl.sh -u Hypadmin -p mypassword -s localhost backup-databases.msh
```with a file backup-databases.msh like```
export database P\_BSO.Plan1 level0 data to data\_file 'ExpLev0\_P\_BSO.Plan1';
```  BUT if you need to **export both metadata and data**, you should run the command```
MAXL> alter database P\_BSO\_D.Plan1 force archive to file 'P\_BSO\_D.Plan1.arc';
```