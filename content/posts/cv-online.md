---
title: 'CV online'
date: Wed, 12 Sep 2007 09:54:21 +0000
draft: false
tags: ['CV', 'Me', 'Work']
---

Ciao Ho messo online il mio curriculum vitae. Se cercate qualcuno che aiuti la vostra Azienda (a Milano, Lecco o Brianza) a traghettare verso un'informatizzazione libera e + economica (grazie a Linux e ai programmi OpenSource) io potrei essere la persona giusta per voi... [http://matteoredaelli.wordpress.com/cv/](http://matteoredaelli.wordpress.com/cv/)
