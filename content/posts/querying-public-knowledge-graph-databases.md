---
title: 'Querying public knowledge graph databases'
date: Sat, 18 Aug 2018 19:54:22 +0000
draft: false
tags: ['Sparql', 'Graph', 'Database', 'prolog', 'Programming languages']
---

You can query public knowledge graph databases (like [wikidata.org](https://wikidata.org) and [dbpedia.org](https://dbpedia.org)) using [SPARQL](https://en.wikipedia.org/wiki/SPARQL). For instance for extracting all "known" programming languages, you can use the query
```
SELECT ?item ?itemLabel WHERE {
 ?item wdt:P31 wd:Q9143.
 SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO\_LANGUAGE],en". }
}
LIMIT 1000
```

There are also SPARQL clients for most of programming languages. With (swi) prolog you can easily run
```
\[library(semweb/sparql\_client)\].
sparql\_query('SELECT ?item ?itemLabel WHERE {?item wdt:P31 wd:Q9143. SERVICE wikibase:label { bd:serviceParam wikibase:language "\[AUTO\_LANGUAGE\],en". }} LIMIT 1000', Row, \[ scheme(https),host('query.wikidata.org'), path('/sparql')\]).
```
