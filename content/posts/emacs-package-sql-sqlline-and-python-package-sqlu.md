---
title: 'Emacs package sql-sqlline and python package sqlu'
date: Mon, 04 Nov 2019 15:27:07 +0000
draft: false
tags:
- emacs
- sql
- python
- sqlu
---

Few days ago I published the following two opensource packages:

*   [sql-sqlline](https://melpa.org/#/sql-sqlline): an emacs package that extends sql package for quering less known databases (aws redshift, aws athena, ..)
*   [sqlu](https://pypi.org/project/sqlu/): a python package for extracting tables from sql select statements
---
### Comments:
####
[buy Andriol](https://bulksteroid.com/ "moseconey@arcor.de") - <time datetime="2020-03-10 20:27:03">Mar 2, 2020</time>

I don't know if it's just me or if perhaps everybody else experiencing issues with yor site. It appears as though some of the written text on your posts are running off the screen. Can somebody else please comment and let me know if this is happening to them as well? This could be a issue with my internet browser because I've had this happen before. Many thanks buy Andriol
<hr />
