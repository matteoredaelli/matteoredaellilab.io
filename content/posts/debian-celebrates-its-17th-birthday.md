---
title: 'Debian celebrates its 17th birthday'
date: Tue, 24 Aug 2010 11:20:46 +0000
draft: false
tags: ['Debian', 'Linux', 'Me', 'OpenSource']
---

[![](http://thank.debian.net/static/cakedebian.png "Debian")](http://www.debian.org/)[Debian](http://www.debian.org/) celebrates its 17th birthay! Read the [official news](http://www.debian.org/News/2010/20100816)..