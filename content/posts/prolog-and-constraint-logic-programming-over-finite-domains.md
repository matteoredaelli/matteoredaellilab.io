---
title: 'Prolog and Constraint Logic Programming over Finite Domains'
date: Mon, 06 Jan 2020 22:11:34 +0000
draft: false
tags:
- Prolog
- Programming
---

I like Prolog and in these days I have studied the library CLP(FD).

For instance it is easy to write a simple code for solving "How many men and horses have 8 heads and 20 feet?". You write the rules and contraints and Prolog will find the solution for you

```
men_and_horses(Men, Horses):-
	Men in 0..10,
	Horses in 0..10,
	Men + Horses #= 8, %% heads must be 8
	Men * 2 + Horses * 4 #= 20. %% feet must be 20

?- men_and_horses(Men, Horses).
 Men = 6,
 Horses = 2.
```

"clp(fd) is useful for solving a wide variety of find values for these variables problems. Here are some broad categories of problems that clp(fd) can address:

*   Scheduling problems, like, when should we do what work in this factory to make these products?
*   Optimization problems, like which mix of products should we make in this factory to maximize profits?
*   Satisficing problems, like finding an arrangement of rooms in a hospital that meets criteria like having the operating theater near the recovery rooms, or finding a set of vacation plans the whole family can agree on.
*   Sequence problems, like finding a travel itinerary that gets us to our destination.
*   Labeling problems, like Sudoku or Cryptarithm puzzles
*   .... " \[See [what\_is\_clp\_fd\_good\_for](http://www.pathwayslms.com/swipltuts/clpfd/clpfd.html#_what_is_clp_fd_good_for)\]
