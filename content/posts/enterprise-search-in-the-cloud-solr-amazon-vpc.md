---
title: 'Enterprise search in the cloud (Solr & Amazon VPC)'
date: Wed, 06 Mar 2013 16:33:32 +0000
draft: false
tags: ['Me']
---

![](http://2.bp.blogspot.com/-NKSMzYuxnEU/UTYI0lNYs6I/AAAAAAAAAxs/k5wEfpBr8Y4/s640/Apache+SolrCloud+Implementation+on+Amazon+EC2+AWS+Cloud.jpg)

"Recently \[Harish Ganesan\] architected a 13+ EC2 Node(s) SolrCloud Shard built in with HA and scalability inside Amazon Virtual Private Cloud. Since the base product engineered had both on-premise and SAAS edition, we were unfortunately not able to use Amazon CloudSearch as it may cause AWS dependency in this case in product tech stack. This Solr architecture was secured using Amazon VPC, Private subnets, Access controls, AWS Security groups and IAM. The SolrCloud cluster was designed to accommodate index data in memory and some search data overflowing to EBS disk. The Cluster was designed to handle ~100 GB of distributed index data and another 200+ GB of Search data on EBS disks. The Web/App tier, Business and Back ground tier will access the SolrCloud deployed in private subnet. In this post I will be sharing my experience, learning, best practice and tips on architecting SolrCloud on AWS platform."

Read the full articles at

[Introduction to Apache SolrCloud on AWS](http://harish11g.blogspot.in/2013/03/Introduction-Apache-Solrcloud-on-Amazon-EC2-AWS.html)

[Apache SolrCloud Implementation on Amazon VPC](http://harish11g.blogspot.in/2013/03/Apache-Solr-cloud-on-Amazon-EC2-AWS-VPC-implementation-deployment.html)