---
title: '#Hadoop Search with Apache #Solr'
date: Fri, 03 Oct 2014 19:35:08 +0000
draft: false
tags: ['Me']
---

The two top [Hadoop](http://hadoop.apache.org/) distributions ([Cloudera](http://www.cloudera.com) and [Hortonworks](http://hortonworks.com) but remember that Hadoop is a Free Software and many companies do not pay anything for using it!) include [Apache Solr](http://lucene.apache.org/solr/) as Hadoop search tool See [apache-solr-hadoop-search](http://hortonworks.com/blog/hdp-2-1-apache-solr-hadoop-search/) article and the following two presentations from the two vendors [![](http://hadoop.apache.org/images/hadoop-logo.jpg)](http://hadoop.apache.org/)   \[slideshare id=35810888&doc=solr-2-140612162029-phpapp02\] \[slideshare id=24255985&doc=hadoopplussolrbigdatasearch-130715115557-phpapp02\]   See also the [Natural Language Processing and Sentiment Analysis for Retailers using HDP and ITC Infotech Radar](http://hortonworks.com/hadoop-tutorial/nlp-sentiment-analysis-retailers-using-hdp-itc-infotech-radar/) article [![](http://hortonworks.com/wp-content/uploads/2014/09/RADAR-Framework.png)](http://hortonworks.com/hadoop-tutorial/nlp-sentiment-analysis-retailers-using-hdp-itc-infotech-radar/)