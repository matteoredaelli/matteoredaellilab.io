---
title: 'TwitterPopularTags.scala example of Apache Spark Streaming in a standalone project'
date: Thu, 22 Oct 2015 20:12:01 +0000
draft: false
tags: ['twitter', 'spark', 'apache', 'scala']
---

![](http://spark.apache.org/docs/latest/img/spark-logo-hd.png) This is an easy tutorial of using [Apache Spark Streaming](http://spark.apache.org/docs/latest/streaming-programming-guide.html) with [Scala language](http://www.scala-lang.org/) using the official  [TwitterPopularTags.scala](https://github.com/apache/spark/blob/master/examples/src/main/scala/org/apache/spark/examples/streaming/TwitterPopularTags.scala) example and putting it in a standalone sbt project.   In few minutes you will be able to receive streams of tweets and manipulating then in realtime with  [Apache Spark Streaming](http://spark.apache.org/docs/latest/streaming-programming-guide.html)

*   Install [Apache Spark](http://spark.apache.org) (I used 1.5.1)
*   Install [sbt](http://www.scala-sbt.org/)
*   git clone [https://github.com/matteoredaelli/TwitterPopularTags](https://github.com/matteoredaelli/TwitterPopularTags)
*   cd TwitterPopularTags
*   cp twitter4j.properties.sample twitter4j.properties
*   edit twitter4j.properties
*   sbt package
*   spark-submit --master local --packages "org.apache.spark:spark-streaming-twitter\_2.10:1.5.1" ./target/scala-2.10/twitterpopulartags\_2.10-1.0.jar italy
