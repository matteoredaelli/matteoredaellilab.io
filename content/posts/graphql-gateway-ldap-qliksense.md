---
title: Using a GraphQL gateway for backend services (Active Directory, AWS and Qliksense Api samples)
date: 2020-10-11
draft: false
tags:
- clojure
- graphQL
- web service
- ldap
- active directory
- qliksense
- aws
---
Complex web sites read and write data from/to several backend systems using different interfaces (sql, soap , rest, rpc,..).
But it could be simpler and useful to create a single endpoint and interface for all the backends.

![](https://d2908q01vomqb2.cloudfront.net/fc074d501302eb2b93e2554793fcaf50b3bf7291/2020/10/12/Conceptual-representation-of-API-facade-pattern.png)

With [GraphQL](https://graphql.org/) the frontend applications get from the backends only the list of fields they need and do not receive the static list of the fields provided by the soap/rest services.

I played with graphql and [Walmart lacinia](https://medium.com/walmartglobaltech/open-sourcing-lacinia-our-graphql-library-for-clojure-96a4ce5fc7b8) implementing one GraphQL backend for LDAP/Active Directory and one for Qliksense Repository rest api.

When you query an ldap server asking for members of a group, you receive the list of distinguishedNames of the members (see below the values of the field "member"). With GraphQL you can add other fields (see below "memberObjects") for retreiving more useful info.

I also added other "meta" fields like ip addresses for computer objects

Sample ldap query:

```graphql
{
	ldap_objects(
		searchdn: "dc=redaelli,dc=org",
		filter: "(sAMAccountName=qliksense_milano)")
	{
		dn
		member
		memberObjects {
			displayName
			sAMAccountName
			mail
		}
	}
}
```

Sample result:

```json
{
  "data": {
	"ldap_objects": [
	  {
		"dn": "CN=Qliksense_Milano,OU=MngScope,OU=IT,DC=redaelli,DC=org",
		"member": [
		  "CN=user1,OU=Standard GUI Users,DC=redaelli,DC=org",
		  "CN=user2,OU=Standard GUI Users,DC=redaelli,DC=org"],
		"memberObjects": [
		  {
			"displayName": "User1",
			"sAMAccountName": "user1",
			"mail": "name1.lastname1@redaelli.org"
		  },
		  {
			"displayName": "User2",
			"sAMAccountName": "user2",
			"mail": "name2.lastname2@redaelli.org"
		  }]
```

Sample ldap query extracting computers and IP addresses

```graphql
{
  ldap_objects(
	system: "group",
	searchdn: "dc=group,dc=redaelli,dc=org",
	filter: "(&(objectClass=computer)(sAMAccountName=dmcAAA*))"
  )
  {
	operatingSystem
	operatingSystemVersion
	dNSHostName
		ipAddresses
  }
}
```

Result:

```json
{
  "data": {
	"ldap_objects": [
	  {
		"operatingSystem": "Windows Server 2016 Datacenter",
		"operatingSystemVersion": "10.0 (14393)",
		"dNSHostName": "DMCAAAW.group.redaelli.org",
		"ipAddresses": [
		  "20.12.9.4"
		]
	  },
```

Sample qliksense query:

```graphql
{
  qliksense_streams(
	system: "myqlikserver"
	filter:"name eq 'Milan'") {
	id
	name
	customProperties {
	  value
	  definition {
		name
	  }
	}}}
```

Result:

```json
{
  "data": {
	"qliksense_streams": [
	  {
		"id": "10a842ba-cdda-48fa-93f6-z53c92cdad61",
		"name": "Milan",
		"customProperties": [
		  {
			"value": "Qliksense_Milan",
			"definition": {
			  "name": "GroupAccess"
		  }}
	  ]}]}}
```

The libraries can be found at

- https://github.com/matteoredaelli/lacinia-backend-aws/ (poc)
- https://github.com/matteoredaelli/lacinia-backend-ldap/ (usable)
- https://github.com/matteoredaelli/lacinia-backend-qliksense/ (usable)
