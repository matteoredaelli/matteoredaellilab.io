---
title: 'Building a Chat Bot...'
date: Sat, 21 Jan 2017 15:59:31 +0000
draft: false
tags:
- chatbot
---

\[last update: June, 5 2017\] My next project will be a Chat Bot.. Starting points are:

*   [AIML (](https://en.wikipedia.org/wiki/AIML)**[Artificial Intelligence](https://en.wikipedia.org/wiki/Artificial_intelligence "Artificial intelligence") Markup Language**) standard
*   [https://messengerplatform.fb.com/](https://messengerplatform.fb.com/) \[from Facebook\]
*   [https://dev.botframework.com/](https://dev.botframework.com/)  \[from Microsoft\]
*   [https://www.rivescript.com/](https://www.rivescript.com/)
*   [https://sourceforge.net/projects/chatscript/](https://sourceforge.net/projects/chatscript/)
*   [Chatterbot](http://chatterbot.readthedocs.io)

There are also some useful softwares as a service like

*   [Api.ai](https://api.ai/) (Google)
*   [Wit.ai](https://wit.ai/) (Facebook)
*   [LUIS](https://www.luis.ai/) (Microsoft)
*   [Watson](https://www.ibm.com/watson/) (IBM)
*   [Lex](https://aws.amazon.com/lex/) (Amazon)

And  alternatives:

*   [rasa.ai](https://rasa.ai/)

Useful ML libraries:

*   [spacy.io](https://spacy.io/)
*   [Tensorflow](https://www.tensorflow.org/)
*   [tflearn](http://tflearn.org/)