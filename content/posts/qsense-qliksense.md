---
title: qsense python library and command line tool for qliksense
date: 2021-03-16
draft: false
tags:
- qliksense
- command line
- python
---

## qSense

[qsense](https://github.com/matteoredaelli/qsense) is an python library and command line tool for qliksense

### Some useful commands

#### export_remove_old_apps

Export (published or passing any other filter) applications to qvd files

	qsense export_delete_old_apps qliksense.redaelli.org ~/certificates/client.pem  --target_path '/tmp' --modified_days=300 --last_reload_days=300

#### deallocate_analyzer_licenses_for_professionals

Deallocate analyzer license fom users with a professional license

	qsense deallocate_analyzer_licenses_for_professionals qliksense.redaelli.org ~/certificates/client.pem --nodryrun

#### delete_removed_exernally_users

Delete users that were removed externally (from active directory?)

	qsense delete_removed_exernally_users qliksense.redaelli.org ~/certificates/client.pem GROUP --nodryrun
