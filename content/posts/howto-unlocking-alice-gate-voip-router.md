---
title: 'Howto unlock Alice Gate VoIP router'
date: Wed, 25 Sep 2013 20:43:28 +0000
draft: false
tags: ['Me']
---

![](http://www.p2warticles.com/wp-content/uploads/2012/04/alice-gate-voip-2-plus-wifi.jpg)

Following the suggestions from [http://www.p2warticles.com/2012/04/menu-avanzato-telnet-alice-wifi-voip-2-plus/](http://www.p2warticles.com/2012/04/menu-avanzato-telnet-alice-wifi-voip-2-plus/) I unlocked my Pirelli Broadband Solutions / Telecom Italia Alice Gate VoIP router **agpf\_4.5.2** with the following steps 1) downloading the backdoor tool (for linux and windows) 2) generating the payload using the mach address of my router

matteo@debian:~/backdoor\_agpf\_4.5.2$ ./backdoor 18:12:2S:E1:A1:B1

3) sending the payload to my router with the linux command (p=XXX where XXX is the payload previously generated)

mz -v eth0 -d 0 -t ip "proto=255,p=3a:b8:69:57:b6:6c:2a:a1"

 After these few steps I was able to login via telnet to my router with the account admin/riattizzati and going on... [![admin-menu](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/09/admin-menu.png)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/09/admin-menu.png)