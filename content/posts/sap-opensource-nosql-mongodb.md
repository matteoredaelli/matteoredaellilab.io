---
title: 'SAP OpenSource NOSQL MongoDB'
date: Sat, 08 Oct 2011 12:03:29 +0000
draft: false
tags: ['MongoDB', 'Nosql', 'OpenSource', 'SAP']
---

**MongoDB Selected as the Core Content Management Component of SAP's Platform-as-a-Service (PaaS) Offering** "MongoDB's Flexibility and Scalability Will Enable SAP to Scale Its Content Management Service on Its PaaS to Meet Customer Demand While Managing Data From Different Applications" Read the full [marketWatch article](http://www.marketwatch.com/story/mongodb-selected-as-the-core-content-management-component-of-saps-platform-as-a-service-paas-offering-2011-09-15).
