---
title: 'Archlinux and Docker for my Raspberry PI2'
date: Sat, 28 Mar 2015 21:30:49 +0000
draft: false
tags:
- raspberry
- docker
- archlinux
- opensource
---

[![](http://www.raspberrypi.org/wp-content/uploads/2015/01/Pi2ModB1GB_-comp-500x283.jpeg)](http://www.raspberrypi.org/) What is the best linux distribution for [Raspberry PI2](www.raspberrypi.org)? I started with [Raspian](http://www.raspbian.org/) (Debian is my preferred Linux distribution for servers, desktops and laptops) but [docker](https://www.docker.com/) didn't work. ![](http://archlinuxarm.org/sites/default/files/wikilogo_0_0.png)But with [Archlinux](http://archlinuxarm.org/platforms/armv7/broadcom/raspberry-pi-2) it works fine. How to create a docker images with Archlinux & RPI2? See [http://linucc.github.io/docker-arch-rpi2/](http://linucc.github.io/docker-arch-rpi2/) [![](http://www.raspbian.org/static/common/raspbian_logo.png)![](https://d3oypxn00j2a10.cloudfront.net/0.16.0/img/homepage/docker-logo-cutoff.png)](https://www.docker.com/) [matteoredaelli/docker-karaf-rpi](https://github.com/matteoredaelli/docker-karaf-rpi) is the first docker image I have created. Below my docker info output:

\[root@raspi1 ~\]# docker info Containers: 4 Images: 9 Storage Driver: aufs Root Dir: /var/lib/docker/aufs Backing Filesystem: extfs Dirs: 17 Execution Driver: native-0.2 Kernel Version: 3.18.10-1-ARCH Operating System: Arch Linux ARM CPUs: 1 Total Memory: 432.8 MiB Name: raspi1 ID: UOFM:E7CP:2OTL:VTTM:QRP2:JNJ7:UFCI:2MDE:AOYQ:MGTM:Q25F:FL37 WARNING: No memory limit support WARNING: No swap limit support

Below some docker survival commands:```
docker run -i -t --name karaf \\
           -p 1099:1099 -p 8101:8101 \\
           -p 44444:44444 -v /apps/karaf-deploy:/deploy \\
           matteoredaelli/karaf-docker-rpi /bin/bash
docker start karaf
docker stop karaf
docker exec -it karaf bash
docker top
docker ps
docker ps -a
docker images



```
