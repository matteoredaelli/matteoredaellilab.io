---
title: 'Retreiving and analyzing Facebook data with R'
date: Thu, 21 Nov 2013 20:17:46 +0000
draft: false
tags: ['R']
---

[![network_plot](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/11/network_plot.png)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/11/network_plot.png)As suggested by [Julianhi's Blog post](http://thinktostart.wordpress.com/2013/11/19/analyzing-facebook-with-r/), I installed the [CRAN](http://cran.stat.unipd.it/web/packages/available_packages_by_name.html) **[Rfacebook](http://cran.r-project.org/web/packages/Rfacebook/index.html)** package for the [R statistical environment](http://www.r-project.org/) and I created a sample [igraph](http://igraph.sourceforge.net/) with 20 of my "facebook friends"

\## See examples for fbOAuth to know how token was created.

\## Getting my network of friends

load("fb\_oauth")

mat <- getNetwork(token=fb\_oauth, format="adj.matrix")

library(igraph)

network <- graph.adjacency(mat, mode="undirected")

pdf("network\_plot.pdf")

plot(network)

dev.off()
---
### Comments:
#### 
[Jbovnd](https://jbovnd.com/ "aja.stonham@gmail.com") - <time datetime="2019-12-22 14:07:50">Dec 0, 2019</time>

I do agree with all of the ideas you've offered to your post. They are really convincing and will definitely work. Still, the posts are very short for novices. May just you please prolong them a little from next time? Thank you for the post.
<hr />
#### 
[judi slot uang asli](http://www.xcusa.net/ "aurora_lovelace@care2.com") - <time datetime="2020-05-05 02:48:45">May 2, 2020</time>

Hi there We are so thrilled I discovered yopur website, I definitely found you by error, while I was searching Yahoo for somethhing otherwise, Anyhow I amm below now and would simply like to ssay many thanks for a tremendous article and an all circular exciting blog (I furthdrmore love the theme/design), I actually don't have time to be able to proceed through it all from the minute but My partner and i have saved it as well as added in yoiur RSS OR ATOM feeds, then when I pssess time I will get returning to read more, Remember to do carry on thhe amazikng job.
<hr />
