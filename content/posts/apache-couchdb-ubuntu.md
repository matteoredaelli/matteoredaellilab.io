---
title: 'Apache CouchDB ~ Ubuntu'
date: Sat, 31 Oct 2009 10:14:47 +0000
draft: false
tags: ['Erlang', 'Me', 'OpenSource', 'Ubuntu']
---

![](http://damienkatz.net/pics/ubuntu_couchdb-thumb-400x298.png "CouchDB & Ubuntu")"[Ubuntu](http://www.ubuntu.com/) 9.10 Karmic Koala has just been released. This is big news as this version includes Apache CouchDB, used as a replicable database by desktop apps. This means [CouchDB](http://couchdb.apache.org/) will be on over 10 million desktops" \[from [Damien Katz](http://damienkatz.net/2009/10/koala_on_the_loose.html)\] [Erlang tutorial](http://learnyousomeerlang.com/)