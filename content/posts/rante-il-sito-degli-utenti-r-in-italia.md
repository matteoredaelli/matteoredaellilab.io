---
title: 'Rante, il sito degli utenti R in Italia'
date: Wed, 15 Sep 2010 19:40:46 +0000
draft: false
tags: ['OpenSource', 'R']
---

[![R statistica in Italia](http://rante.org/wp-content/uploads/2010/08/Rug2.png "R statistica in Italia")](http://rante.org)

"[Rante](http://rante.org/) è una comunità di utenti appassionati di R, un ambiente di programmazione open source ideato principalmente per l’analisi statistica dei dati. Si fonda nella condivisione della filosofia del software libero e nella convinzione che avere un punto di ritrovo e scambio comune, non solo virtuale, possa portare benefici per tutti coloro che sono interessati all’uso di questo ambiente di programmazione..." \[[rante.org](http://rante.org/about/)\]
