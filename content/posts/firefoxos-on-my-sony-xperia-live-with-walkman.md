---
title: 'FirefoxOS on my Sony Xperia Live with Walkman (LWW)'
date: Tue, 30 Jul 2013 19:13:17 +0000
draft: false
tags: ['Me']
---

![](http://4.bp.blogspot.com/-KWnbBNGwA0g/UeDwmeaO_JI/AAAAAAAAT14/O3Z29WMYji4/s1600/1.png)

\[update 2013-11-06\] how to install [FireFoxOS](http://www.mozilla.org/en-US/firefox/os/) 1.3

Unlock your phone,

download "flashtool" tool

download latest FireFoxOS build from [here](http://forum.xda-developers.com/showthread.php?t=2422687)

flash boot.img included in the zip

copy the zip to the sdcard

boot in recovery mode and flash the zip

good lucks... ;-)

\--------------------------

First look at **[FirefoxOS](http://www.mozilla.org/en-US/firefox/os/)** on my **Sony XPERIA 2011 Live With Walkman** (LWW, wt19i, coconut) following suggestions from

[http://www.galaxy4gaming.in/2013/07/firefox-os-for-xperia-2011-mini-and.html](http://www.galaxy4gaming.in/2013/07/firefox-os-for-xperia-2011-mini-and.html)

See also

[http://forum.xda-developers.com/showthread.php?p=43803195](http://forum.xda-developers.com/showthread.php?p=43803195)

![](http://developer-static.se-mc.com/wp-content/blogs.dir/1/files/2013/02/FFOS_arch.jpg)