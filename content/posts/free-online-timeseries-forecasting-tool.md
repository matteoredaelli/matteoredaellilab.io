---
title: 'Free Online Timeseries forecasting tool'
date: Sat, 09 Aug 2014 11:59:44 +0000
draft: false
tags: ['Me']
---

![](https://raw.githubusercontent.com/matteoredaelli/predictoR/master/doc/predictoR-1.png) An Online demo for the Opensource tool ([predictoR](https://github.com/matteoredaelli/predictoR) based on [ltp R library](http://ltp.googlecode.com/)) about forecasting / long term prediction of time series (any periods: daily, weekly, monthly, semesters, yearly,..) is available at [rapid.tips](http://rapid.tips/)! ![](https://raw.githubusercontent.com/matteoredaelli/predictoR/master/doc/predictoR-2.png)