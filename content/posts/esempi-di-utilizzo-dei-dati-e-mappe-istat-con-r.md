---
title: 'Esempi di utilizzo dei dati e mappe Istat con R'
date: Mon, 20 Dec 2010 20:27:13 +0000
draft: false
tags: ['Carate Brianza', 'Istat', 'Me', 'OpenSource', 'R']
---

\[youtube\]http://www.youtube.com/watch?v=cUyI6PT7QT4\[/youtube\] Trovate su [R-Istat](https://github.com/matteoredaelli/r-istat) alcuni semplici esempi di utilizzo con [R](http://www.redaelli.org/matteo-blog/projects/r-statistics/) dei dati e mappe cartografiche forniti liberamente dall'Istat. In questo post un video e un grafico di esempio.. ![](https://github.com/matteoredaelli/r-istat/raw/master/sample/CarateBrianza_popolazione_vedovi.png "carate brianza dati 2010")![](https://github.com/matteoredaelli/r-istat/raw/master/sample/Coniugati-040.png "istat coniugati di 40 anni in italia") .