---
title: 'A case study of adopting Bigdata technologies in your company'
date: Fri, 13 Mar 2015 20:52:33 +0000
draft: false
tags: ['Camel', 'Hadoop', 'Linux', 'Me', 'OpenSource', 'RaspberryPI', 'Spark']
---

[Bigdata](http://en.wikipedia.org/wiki/Big_data) projects can be very expensive and can easily fail: I suggest to start with a small, useful but not critical project. Better if it is about unstructured data collection and batch processing. In this case you have time to get practise with the new technologies and the [Apache Hadoop](http://hadoop.apache.org/) system can have not critical downtimes. At home I have the following system running on a small [Raspberry PI](http://www.raspberrypi.org/): for sure it is not fast ;-) At work I introduced [Hadoop](http://hadoop.apache.org/) just few months ago for collecting web data and generating daily reports. [![Competitor_Analysis_BigData](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/03/Competitor_Analysis_BigData.png)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/03/Competitor_Analysis_BigData.png)