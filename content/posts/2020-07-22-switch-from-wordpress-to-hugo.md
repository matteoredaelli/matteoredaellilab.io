---
title: Switching from Wordpress to a static website
date: 2020-07-20
draft: false
tags: 
- hugo
- wordpress
- opensource
- markdown
---
I migrated my Blog site from Wordpress to a faster static web site using  markdown files and [Hugo](https://gohugo.io/).
