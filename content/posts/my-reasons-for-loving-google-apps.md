---
title: 'Some Reasons for Loving Google Apps'
date: Fri, 25 Sep 2009 11:22:38 +0000
draft: false
tags: ['Collaboration', 'Google', 'Intranet', 'Me']
---

[![](http://docs.google.com/drawings/image?id=stMoHQ71hJ6503vKbvxcz8w&w=571&h=435&rev=1&crop=35814%2C5715%2C217551%2C165735)](http://docs.google.com/present/view?id=dfr68gz6_172gb8q5wgg) A **[quick presentation](http://docs.google.com/present/view?id=dfr68gz6_172gb8q5wgg)** about why people & employees & IT managers & companies should love and adopt [**Google Apps**](http://www.google.com/apps/) instead of other old style **Intranets** (MS Sharepoint, Sap Portal, .. that require the (not user friendly) programmers's actions checkin/checkout) and **Email Suites** (MS Exchange)