---
title: 'Latest packages for Elixir Haskell Lua dotNet NodeJs Php Python R Ruby Scala'
date: Thu, 10 Aug 2017 08:45:20 +0000
draft: false
tags: ['Me']
---

The site [packages.matteoredaelli.eu](http://packages.matteoredaelli.eu/) collects the last updated packages for most of programming languages (at the moment Elixir Haskell Lua dotNet NodeJs Php Python R Ruby Scala)