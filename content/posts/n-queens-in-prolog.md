---
title: 'N queens problem in Prolog'
date: Fri, 10 Jan 2020 17:59:40 +0000
draft: false
tags: 
- prolog
- Constraint programming
---

![](http://www.redaelli.org/matteo-blog/wp-content/uploads/2009/01/8queens.png)

Below the [N Queens problem](https://en.wikipedia.org/wiki/Eight_queens_puzzle) solved in [Prolog](https://en.wikipedia.org/wiki/Prolog) and [Constraint Logic Programming over Finite Domains](https://www.swi-prolog.org/pldoc/man?section=clpfd) library

```prolog
:- use_module(library(clpfd)).

n_queens(N, Queens) :-
        length(Queens, N),
        Queens ins 1..N,
	all_different(Queens), %% the queens must be in different columns
        different_diagonals(Queens).

different_diagonals([]).
different_diagonals([Q|Queens]) :- different_diagonals(Queens, Q, 1), different_diagonals(Queens).

different_diagonals([], _, _).
different_diagonals([Q|Queens], Q0, Distance) :-
        abs(Q0 - Q) #\= Distance,
        NewDistance #= Distance + 1,
        different_diagonals(Queens, Q0, NewDistance).

/* 
   Queens is the list of columns of the queens: the corresponding rows are the position of the elements/columns in the list Queens

   Examples:

   ?- n_queens(8, Queens), labeling([ff], Queens).
   %@ Queens = [1, 5, 8, 6, 3, 7, 2, 4] ;
   %@ Queens = [1, 6, 8, 3, 7, 4, 2, 5] .

   The result chess cells are, for instance, [1,1], [2,5], [3,8], [4,6], [5,3], [6,7], [7,2], [8,4]

   Suggestion from https://www.swi-prolog.org/pldoc/man?section=clpfd-n-queens
 */ 
```
