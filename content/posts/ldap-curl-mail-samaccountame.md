---
title: 'Howto quickly extract LDAP attributes for a list of users'
date: 2020-09-07
draft: false
tags:
- ldap
- active directory
- curl
- command line
- bash

---

I had a list of email addresses and my boss asked me to extract the login names (SamAccountname attribute in Active Directory). I quickly did it with:

```
  cat mails.txt | while read mail
  do
	echo /usr/bin/curl  -s --proxy \"\" -u \"MYDOMAIN\\myuser:mypassword\" \"ldap://ldapserver:3268/DC=group,DC=mycompany,DC=com?sAMAccountName?sub?(mail=${mail})\"
  done  | bash | grep "sAMAccountName:" | cut -f2 -d":" | sed -e 's/^ /"/' -e 's/$/",/'
  ```
