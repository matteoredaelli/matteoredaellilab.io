---
title: 'Dump_tweets.R: how to collect interesting tweets'
date: Sat, 06 Jul 2013 08:45:38 +0000
draft: false
tags:
- R
- twitter
---

Do you need a simple [R](http://www.r-project.org/) script for incremental downloading all tweets related an hashtag (or any words) and saving them to a database? If yes, look at [dump\_tweets.R](https://github.com/matteoredaelli/dump_tweets.R) project: feel free to clone my repository and push any fixes or new features Usage:

./dump\_tweets.R --db /var/www/dump\_tweets.R --add mysearches.csv

./dump\_tweets.R --db /var/www/dump\_tweets.R --show

./dump\_tweets.R --db /var/www/dump\_tweets.R

./dump\_tweets.R --db /var/www/dump\_tweets.R --remove opensource

Enjoy
