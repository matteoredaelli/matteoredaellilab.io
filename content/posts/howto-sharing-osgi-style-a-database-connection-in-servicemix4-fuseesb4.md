---
title: 'Howto sharing (osgi style) a database connection in Servicemix4 / FuseEsb4'
date: Sun, 07 Jun 2009 12:58:58 +0000
draft: false
tags: ['Me', 'OpenSource', 'Servicemix']
---

<?xml version="1.0" encoding="UTF-8"?> <beans xmlns="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:camel-osgi="http://activemq.apache.org/camel/schema/osgi" xmlns:osgi="http://www.springframework.org/schema/osgi" xsi:schemaLocation=" http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd http://www.springframework.org/schema/osgi http://www.springframework.org/schema/osgi/spring-osgi.xsd http://activemq.apache.org/camel/schema/spring http://activemq.apache.org/camel/schema/spring/camel-spring.xsd http://activemq.apache.org/camel/schema/osgi http://activemq.apache.org/camel/schema/osgi/camel-osgi.xsd"> <bean id="moodleDS"> <property name="driverClassName" value="com.mysql.jdbc.Driver" /> <property name="url" value="jdbc:mysql://localhost:3306/moodle" /> <property name="username" value="root" /> <property name="password" value="" /> </bean> <osgi:service id="moodleDSOsgiService" ref="moodleDS" interface="javax.sql.DataSource" /> </beans> See my [Google Document](http://docs.google.com/View?id=dfr68gz6_71gqrcg5dp) for details!