---
title: 'E'' notte per l''ultima volta'
date: Fri, 14 Sep 2007 06:28:08 +0000
draft: false
tags: ['Poesie', 'Uncategorized']
---

> _E' notte per l'ultima volta.__Per l'ultima volta le tue mani_ _si raccolgono sul mio corpo._ _Domani sara' autunno._ _Insieme seduti in terrazza_ _guarderemo le foglie secche spargersi sul villaggio_ _come lettere che bruciano,_ _una per una,_ _in case separate._

Louise Gluck, quotidiano [CITY](http://www.city.it)