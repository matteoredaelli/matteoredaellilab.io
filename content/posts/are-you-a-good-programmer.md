---
title: 'Are you a good programmer?'
date: Sat, 20 Aug 2011 06:24:14 +0000
draft: false
tags: ['Me', 'Programming']
---

![](http://techiferous.com/wp-content/uploads/2011/08/philosopher.png "http://techiferous.com/wp-content/uploads/2011/08/philosopher.png")![](http://techiferous.com/wp-content/uploads/2011/08/inventor.png "http://techiferous.com/wp-content/uploads/2011/08/inventor.png")I have just read a funny article about the kind of good programmers at [techiferous.com](http://techiferous.com/2011/08/are-you-a-good-programmer/): the philosopher, the inventor, the conqueror, the problem solver..