---
title: 'Al via [R]innova di Rante sullo Strategico'
date: Thu, 09 Jun 2011 18:32:01 +0000
draft: false
tags: ['Me', 'OpenSource', 'R']
---

["\[R\]innova](http://rante.org/2011/06/08/rinnova/ "[r]innova & rante") vuole essere la prima esperienza di programmazione condivisa per la Web Application Strategico, un progetto open source per l’analisi di serie storiche. L’obiettivo di questa prima edizione di \[R\]innova è lo sviluppo del layout grafico dei risultati prodotti da [Strategico](http://www.redaelli.org/matteo-blog/projects/r-strategico/).... Si tratta di un concorso con in palio un [..."](http://rante.org/2011/06/08/rinnova/ "R & Rante & STrategico") Leggi l'articolo sul sito [rante.org](http://rante.org/2011/06/08/rinnova/) [![](http://rante.org/wp-content/uploads/2011/06/Strategico_Trials1.png "Strategico")](http://95.224.253.126/strategico/web/ws.html) See [results](http://95.224.253.126/strategico/projects/web-ltp/1200/) of users's tests