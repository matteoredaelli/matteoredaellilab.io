---
title: 'Howto Install Android 4.4.2 to my Sony Live with walkman wt19i'
date: Mon, 10 Jun 2013 20:04:36 +0000
draft: false
tags:
- linux
- android
- sony
---

![](http://wiki.cyanogenmod.org/images/thumb/2/2f/Coconut.png/150px-Coconut.png)

UPDATES:

2014-03-25: look [here at androidlegend.com](http://www.androidlegend.com/sony-live-with-walkman-wt19i-android-4-4-2-kitkat-update-carbon-rom/) for newer [CarbonRom](https://carbon-rom.com/) 4.4.2 builds

[![](https://carbon-rom.com/wp-content/uploads/2013/12/Black-on-Trans.png)](https://carbon-rom.com/wp-content/uploads/2013/12/Black-on-Trans.png)

2014-01-04: see [http://legacyxperia.github.io](http://legacyxperia.github.io/) for unofficial builds of CyanogenMod 11.0 (**Android 4.4.2**)

[![](http://i.imgur.com/2JvwZI4.png)](http://forum.xda-developers.com/showthread.php?t=2545367)

2013-12-11: see [http://legacyxperia.github.io](http://legacyxperia.github.io/) for unofficial builds of CyanogenMod 11.0 (**Android 4.4.1**)

2013-09-01: see [http://legacyxperia.github.io](http://legacyxperia.github.io/) for unofficial builds of CyanogenMod 10.2 (**Android 4.3**)  for legacy Sony Ericsson msm7x30 devices. It is an effort to join all developer forces to bring a stable cm-10.1 rom on these devices which are dropped from official CM

\-.-.-

ORIGINAL\_POST

After installing android tools

[adb](http://wiki.cyanogenmod.org/w/Doc:_adb_intro) and [fastboot](http://wiki.cyanogenmod.org/w/Doc:_fastboot_intro)

on my Linux desktop,

I installed Android 4.2.2 ([Cyanogenmod](http://www.cyanogenmod.org/) distribution) following the steps suggested by the article

[install-sony-live-with-walkman-wt19i-android-4-2](http://www.androidgadgematic.com/2012/12/install-sony-live-with-walkman-wt19i-android-4-2.html)

See also [Coconut\_Info](http://wiki.cyanogenmod.org/w/Coconut_Info)

NB: I also tested an other Android distribution ([Paranoid Android](http://www.paranoid-rom.com/)),

but I was not able to switch the setting menu from tablet layout to a smartphone layout
