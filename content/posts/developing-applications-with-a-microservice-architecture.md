---
title: 'Developing applications with a microservice architecture'
date: Sat, 09 Aug 2014 19:14:39 +0000
draft: false
tags: ['Me']
---

![](http://martinfowler.com/articles/microservices/images/sketch.png) "The microservice architectural style is an approach to developing a single application as a suite of small services, each running in its own process and communicating with lightweight mechanisms, often an HTTP resource API. These services are built around business capabilities and independently deployable by fully automated deployment machinery." Read the full [article](http://martinfowler.com/articles/microservices.html) ![](https://raw.githubusercontent.com/matteoredaelli/twitter-api-ws/master/doc/microservices-clojure.png)   This is my first sample microservices written in [clojure](http://clojure.org/)
