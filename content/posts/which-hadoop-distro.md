---
title: 'A comparison of Hadoop distributions'
date: Thu, 21 May 2015 14:39:17 +0000
draft: false
tags: ['apache', 'hadoop']
---

[![](http://api.ning.com/files/pHhBdYiNnuLDQBdUDfdIGEUdajKWuQbhs7HWwyjB43-W0K*J*rtzeM3-nC6SgcRWRhB*T0731FSgHGnQlJ3q2LeDhViqLOJQ/WhichHadooptable1.jpg?width=750)](http://www.hadoop360.com/blog/hadoop-whose-to-choose) ![](http://api.ning.com/files/pHhBdYiNnuLpHSIDZAXzQ-cko-QSMpYPzWt3CsyHorecQ6iTEbJ-RuGlivQWv27S*ikesLHY7hNoXXaWks9Mf*FjQHvVzWRV/WhichHadooptable4.jpg?width=720) Read all the article at [http://www.hadoop360.com/blog/hadoop-whose-to-choose](http://www.hadoop360.com/blog/hadoop-whose-to-choose)
