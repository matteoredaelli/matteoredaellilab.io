---
title: 'Building a Cloud-Agnostic Serverless infrastructure with Apache OpenWhisk'
date: Thu, 29 Nov 2018 20:29:02 +0000
draft: false
tags:
- serverless
- kubernetes
- openwhisk
- opensource
---

![](http://www.redaelli.org/matteo-blog/wp-content/uploads/2018/11/OW-Abstract-Architecture-Diagram-1024x487.png)

"[Apache OpenWhisk](https://openwhisk.apache.org/) (Incubating) is an open source, distributed Serverless platform that executes functions (fx) in response to events at any scale. OpenWhisk manages the infrastructure, servers and scaling using Docker containers so you can focus on building amazing and efficient applications...

DEPLOY Anywhere: Since Apache OpenWhisk builds its components using containers it easily supports many deployment options both locally and within Cloud infrastructures. Options include many of today's popular Container frameworks such as [Kubernetes](https://github.com/apache/incubator-openwhisk-deploy-kube/blob/master/README.md), [Mesos](https://github.com/apache/incubator-openwhisk-deploy-mesos/blob/master/README.md) and [Compose](https://github.com/apache/incubator-openwhisk-devtools/blob/master/docker-compose/README.md)

ANY LANGUAGES: Work with what you know and love. OpenWhisk supports a growing list of your favorite languages such as **[NodeJS](https://github.com/apache/incubator-openwhisk-runtime-nodejs)**, **[Swift](https://github.com/apache/incubator-openwhisk-runtime-swift)**, **[Java](https://github.com/apache/incubator-openwhisk-runtime-java)**, **[Go](https://github.com/apache/incubator-openwhisk-runtime-go)**, **[Scala](https://github.com/apache/incubator-openwhisk-runtime-java)**, **[Python](https://github.com/apache/incubator-openwhisk-runtime-python)**, **[PHP](https://github.com/apache/incubator-openwhisk-runtime-php)** and **[Ruby](https://github.com/apache/incubator-openwhisk-runtime-ruby)**.

If you need languages or libraries the current "out-of-the-box" runtimes do not support, you can create and customize your own executables as Zip Actions which run on the **[Docker](https://github.com/apache/incubator-openwhisk-runtime-docker/blob/master/README.md)** runtime by using the **[Docker SDK](https://github.com/apache/incubator-openwhisk-runtime-docker/blob/master/sdk/docker/README.md)**. " \[[openwhisk.apache.org](https://openwhisk.apache.org/)\]
