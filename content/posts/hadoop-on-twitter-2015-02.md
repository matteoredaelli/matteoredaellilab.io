---
title: 'Hadoop on Twitter 2015-02'
date: Mon, 02 Mar 2015 10:03:30 +0000
draft: false
tags: ['Me']
---

Below the top media (and other statistics) about "Hadoop" search in Twitter in February 2015

![](https://pbs.twimg.com/media/B978KzvIcAEB0Aw.png)

[![hadoop-2015-02](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/03/hadoop-2015-02-300x225.jpg)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/03/hadoop-2015-02.jpg)