---
title: 'My Networking Survival Kit'
date: Sun, 15 Mar 2020 21:44:38 +0000
draft: false
tags: ['Me']
---

In this small tutorial I'll speak about tunneling, ssh port forwarding, [socks](https://en.wikipedia.org/wiki/SOCKS), [pac files](https://en.wikipedia.org/wiki/Proxy_auto-config), [Sshuttle](https://sshuttle.readthedocs.io/en/stable/how-it-works.html)

![](https://www.howtogeek.com/wp-content/uploads/2017/03/why-is-a-network-tunnel-called-a-tunnel-00.jpg)

I've been using Linux since 1995 but I have never been interested a lot in networking. In these many days of smart working (due to Covid-19) I have found some useful tricks to connect to remote systems that are not directly reachable from my lan/vpn

**Case 1** (port forwarding): I wanted to connect to targethost.redaelli.org at tcp port 10000 but I was not able to reach it directly but only through an other host (tunnelhost.redaelli.org). With the following command I was able to reach the target host connecting to localhost:10000

```
ssh -NL 10000:targethost.redaelli.org:10000 r@tunnelhost.redaelli.org
```

**Case 2** (many port forwarding): I added in the file $HOME/.ssh/config

```
Host tunnelhost
User matteo
Hostname tunnelhost.redaelli.org
LocalForward 10000 192.168.20.152:10000
LocalForward 10001 192.168.40.123:10000
LocalForward 10002 192.168.60.112:10000
```

And after running "ssh -N tunnelhost" I was able to reach the target systems through localhost:10000, localhost:10001 and localhost:10002

**Case 3** (socks5): connecting to many remote hosts using their hostnames (and not localhost). I started a socks server with

```
ssh -D 9999 -q -C -N matteo@tunnelhost.redaelli.org
```

An then I configured it in network settings in Firefox.

A further improvement was to create a "pac" file and set it in the network settings in Firefox. My _pac file_ was

```
function FindProxyForURL(url, host) {
    var useSocks = ["remotehost.redaelli.org", 
                    "remotehost2.redaelli.org"];

    for (var i= 0; i < useSocks.length; i++) {
	if (shExpMatch(host, useSocks[i])) {
	    return "SOCKS localhost:9999 ; DIRECT";
	}
    }
    if (isInNet(dnsResolve(host), "192.168.20.0", "255.255.255.0") ||
        isInNet(dnsResolve(host), "192.168.40.0", "255.255.255.0") ||
        isInNet(dnsResolve(host), "192.168.60.0", "255.255.255.0")
       ) {
	    return "SOCKS localhost:9999 ; DIRECT";
    }
    return "DIRECT";
}
```

**Case 4** ([sshuttle](https://sshuttle.readthedocs.io/en/stable/index.html)): connecting to many remote hosts like native connections (without socks and pac files).

"Sshuttle is not exactly a VPN, and not exactly port forwarding. It’s kind of both, and kind of neither."

"Sshuttle assembles the TCP stream locally, multiplexes it statefully over an ssh session, and disassembles it back into packets at the other end. So it never ends up doing TCP-over-TCP. It’s just data-over-TCP, which is safe."

I installed sshuttle with "apt-get install sshuttle" on my debian laptop and then I ran

```
sshuttle -r matteo@tunnelhost.redaelli.org 192.168.20.0/24 192.168.40.0/24 192.168.60.0/24
```

For sure there are many other more powerful and better solutions. But for the moment these are the one I have used.