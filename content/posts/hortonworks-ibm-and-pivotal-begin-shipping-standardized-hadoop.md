---
title: 'Hortonworks, IBM and Pivotal begin shipping standardized Hadoop'
date: Wed, 15 Apr 2015 09:49:30 +0000
draft: false
tags: ['Me']
---

![](https://hadoop.apache.org/images/hadoop-logo.jpg) "Hortonworks, IBM and Pivotal begin shipping standardized Hadoop. The standardization effort is part of the [Open Data Platform](http://opendataplatform.org/) initiative, which is an industry effort to ensure all versions of [Hadoop](https://hadoop.apache.org/) are based on the same Apache core..". Read all the full [zdnet.com article](http://www.zdnet.com/article/hortonworks-ibm-and-pivotal-begin-shipping-standardized-hadoop/)  This is thanks to the [Apache Ambari](https://ambari.apache.org/) project! 

*   [Cloudera](http://www.cloudera.com/) still goes alone with its old custom solution.
*   And [MapR](https://www.mapr.com/) bets on [Apache Mesos](http://mesos.apache.org/) (see [https://lnkd.in/e\_ZwNn5](https://www.linkedin.com/redirect?url=https%3A%2F%2Flnkd%2Ein%2Fe_ZwNn5&urlhash=-Pdy&_t=commentary-share-link&trk=commentary-share-link))

I also suggest to install Ambari/Hadoop in a [Docker](https://www.docker.com/) container.. the new wave of "virtualization". About Docker & Hadoop: Hortonworks to Acquire SequenceIQ to Speed Hadoop Deployments into the Cloud (docker containers) [http://investors.hortonworks.com/phoenix.zhtml?c=253804&p=irol-newsArticle&ID=2034442](https://www.linkedin.com/redirect?url=http%3A%2F%2Finvestors%2Ehortonworks%2Ecom%2Fphoenix%2Ezhtml%3Fc%3D253804%26p%3Dirol-newsArticle%26ID%3D2034442&urlhash=GYo0&_t=commentUrl)