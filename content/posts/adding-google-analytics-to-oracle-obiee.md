---
title: 'Adding Google Analytics to Oracle OBIEE'
date: Mon, 29 Sep 2014 11:39:23 +0000
draft: false
tags: 
- oracle
- obiee
- google analytics
---

![](http://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Oracle_logo.svg/663px-Oracle_logo.svg.png) I opened a service request to Oracle and they did not provide me an official way to add the Google Analytics javascript code to Oracle OBIEE (release 11.1.1.7): I wanted to add it in only one place and see it in all pages of Oracle Obiee. The solution I found and tested is to add the javascript code (without <scripts> and </scripts>) in the file```
bi\_server1/tmp/\_WL\_user/analytics\_11.1.1/7dezjl/war/res/b\_mozilla/common.js
```Pay attention that the file could be overwritten after any software upgrades
