---
title: 'Apache Tez for Hadoop >= 2.0'
date: Wed, 12 Mar 2014 16:19:16 +0000
draft: false
tags: ['hadoop', 'tez']
---

[![](http://hortonworks.com/wp-content/uploads/2013/10/hadoopstack.png)](http://hortonworks.com/hadoop/tez/) The [**Apache Tez**](http://tez.incubator.apache.org/) project is aimed at building an application framework which allows for a complex directed-acyclic-graph of tasks for processing data. It is currently built atop [Apache Hadoop](http://hadoop.apache.org/) [YARN](http://hortonworks.com/hadoop/yarn/). The 2 main design themes for Tez are:

*   Empowering end users by:

> *   Expressive dataflow definition APIs
> *   Flexible Input-Processor-Output runtime model
> *   Data type agnostic
> *   Simplifying deployment

*   Execution Performance
    *   Performance gains over Map Reduce
    *   Optimal resource management
    *   Plan reconfiguration at runtime
    *   Dynamic physical data flow decisions

[![](http://tez.incubator.apache.org/images/PigHiveQueryOnTez.png)](http://tez.incubator.apache.org/) Old way was: ![](http://tez.incubator.apache.org/images/PigHiveQueryOnMR.png) References:

*   [http://tez.incubator.apache.org/](http://tez.incubator.apache.org/)
*   [http://hortonworks.com/hadoop/tez/](http://hortonworks.com/hadoop/tez/)
*   [http://hadoop.apache.org/](http://hadoop.apache.org/)
