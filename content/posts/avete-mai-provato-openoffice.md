---
title: 'Avete mai provato OpenOffice?'
date: Wed, 03 Oct 2007 14:36:55 +0000
draft: false
tags: ['Office', 'OpenSource', 'Uncategorized']
---

OpenOffice e’ una suite office GRATUITA [scaricabile](http://www.openoffice.org/) liberamente da internet che legge e scrive documenti di Microsoft Office (doc,xls,ppt,..). Stupisce per le similarita’ di aspetto, funzionalita’ e usabilita’ col suo concorrente commerciale…Su [http://www.linux.com/feature/119513](http://www.linux.com/feature/119513) trovate una comparazione tra le due suite circa la creazione di presentazioni… Io da anni uso unicamente OpenOffice per leggere e scrivere documenti di lavoro e uso spesso la funzione di export as PDF…