---
title: 'Varnish for speeding up your websites'
date: Sat, 08 Jun 2013 19:40:20 +0000
draft: false
tags:
- varnish
---

![](https://www.varnish-cache.org/sites/all/themes/varnish_d7/images/banner_1.png)

"Varnish is able to serve objects out of its cache much faster and using far fewer host resources than a web server application.."

Read all the interesting article [Adventures in Varnish](http://blog.bigdinosaur.org/adventures-in-varnish/)

![](http://blog.bigdinosaur.org/images/blog-title-varnish-adventure.jpg)
