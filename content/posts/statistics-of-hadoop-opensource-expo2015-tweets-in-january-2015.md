---
title: 'Statistics of #Hadoop #Opensource #Expo2015 tweets in January 2015'
date: Thu, 05 Feb 2015 13:05:47 +0000
draft: false
tags: ['twitter']
---

How easy is analyzing json twitter data using [Apache Spark](https://spark.apache.org/) and [Apache Hadoop](https://hadoop.apache.org/). Below some examples **Tweets about Expo2015** ![](http://4.bp.blogspot.com/-kJ9bgVLfC0U/VNNKbk2g8PI/AAAAAAAAGh8/b6Edd8PjhLI/s1600/Expo2015-01.jpg) **Tweets about Hadoop** ![](http://4.bp.blogspot.com/-SZLcjO7ko2Q/VNNKCeiyuJI/AAAAAAAAGh0/lTB-kQN2QWc/s1600/hadoop-2015-01.jpg) **Tweets about opensource** ![](http://1.bp.blogspot.com/-HrJVKCq42es/VNNJSSO83XI/AAAAAAAAGhs/51o4c45_14s/s1600/opensource-2015-01.jpg)
