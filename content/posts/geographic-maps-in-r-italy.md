---
title: 'Geographic maps in R: Italy'
date: Mon, 13 Sep 2010 19:09:10 +0000
draft: false
tags: ['Me', 'OpenSource', 'Programming', 'R']
---

As suggested by this [post](http://blog.revolutionanalytics.com/2009/10/geographic-maps-in-r.html), I created an Italian map

[![Italian Map with R and sp package](http://www.redaelli.org/matteo-blog/wp-content/uploads/2010/09/italy-with-r.png "italy-with-r")](http://www.redaelli.org/matteo-blog/wp-content/uploads/2010/09/italy-with-r.png)

with the following [R](http://www.r-project.org/) code using the [sp](http://cran.r-project.org/web/packages/sp/) package and the [GADM](http://gadm.org/) maps

library(sp)

con <- url("http://gadm.org/data/rda/ITA\_adm1.RData")

print(load(con))

close(con)

\# giving random values to the Italian regions (1,2,3,5,1,2,3,4,5,...)

language <- rep(seq(1,5),4)

gadm$language <- as.factor(language)

\# choosing colors

col = rainbow(length(levels(gadm$language)))

spplot(gadm, "language", col.regions=col, main="Italian Regions")