---
title: 'Adding an application (angularjs+rest api) inside a Wordpress site'
date: Tue, 26 Apr 2016 19:33:03 +0000
draft: false
tags:
- angularjs
- spa
- rest
- wordpress
---

![](https://angularjs.org/img/AngularJS-large.png)If you need to integrate an application written with [AngularJS](https://angularjs.org/) and Rest API services in your [wordpress](https://wordpress.org/) website, just create an empy page and edit it in "text" mode with something like```
<!-- the following two lines can be put in the header template --> 
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular.min.js"></script>
<script src="http://your.site.com/app.js"></script>

<div ng-app="myApp" ng-controller="planetController">
       <div >
           <input ng-model="query" placeholder="inserisci una parola" type="text">
            <p><button ng-click="searchV(query)" >Dividi in sillabe</button></p>
       </div>
</div
```A running example is (now, but in the near future I'll switch to a generated static web site) at [http://rapid.tips/site/sillabazione-parole-italiane/](http://rapid.tips/site/sillabazione-parole-italiane/)