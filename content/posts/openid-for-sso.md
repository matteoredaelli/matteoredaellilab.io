---
title: 'OpenID for SSO'
date: Sat, 08 Sep 2007 06:11:56 +0000
draft: false
tags: ['Software', 'openid']
---

OpenID has been getting more and more famous and many OpenSource projects have already adopted it ... [http://openid.net/presentations.bml](http://openid.net/presentations.bml) [http://www.openidbook.com/chapters/OpenIDBook-Draft-13.pdf](http://www.openidbook.com/chapters/OpenIDBook-Draft-13.pdf) There are libraries for the most common programming languages (Python, Ruby, Java, Perl, .Net) [http://openid.net/wiki/index.php/Libraries](http://openid.net/wiki/index.php/Libraries) How does it work? Get an OpenID userid from an OpenID provider (wordpress.com, aol, livejournal,.. [OpenIDServers)](http://openid.net/wiki/index.php/OpenIDServers%29) or from your own provider OpenID (see [Run\_your\_own\_identity\_server](http://openid.net/wiki/index.php/Run_your_own_identity_server), i.e [crowd](http://www.atlassian.com/software/crowd) is used by several big companies but you have to pay for it if you use it not for openSource projects) and then you can use that account (if you want) to enter all other OpenID enables sites ([https://www.myopenid.com/directory](https://www.myopenid.com/directory)) [http://openid.net/pres/2007-kveton-openid-5.pdf](http://openid.net/pres/2007-kveton-openid-5.pdf)
---
### Comments:
#### 
[Matteo](http://www.redaelli.org "matteo.redaelli@gmail.com") - <time datetime="2008-01-10 12:45:11">Jan 4, 2008</time>

Anche Google sceglie OpenID come sistema di Identity e SSO. Vedete http://punto-informatico.it/p.aspx?id=2155653 e http://uk.techcrunch.com/2008/01/09/google-ibm-and-verisign-to-join-openid/ Con la sola userid/password di Google (o di wordpress.com, AOL, o qualsiasi altro provider OpenID) sara’ possibile accedere (in SSO) a tutti i siti OpenID enabled.
<hr />
#### 
[Matteo](http://www.redaelli.org "matteo.redaelli@gmail.com") - <time datetime="2008-05-07 08:10:56">May 3, 2008</time>

Anche sourceforge accetta utenze OpenID! http://openid.net/2008/05/01/sourceforge-allows-openid-logins/
<hr />
#### 
[Matteo](http://www.redaelli.org "matteo.redaelli@gmail.com") - <time datetime="2008-07-10 14:42:08">Jul 4, 2008</time>

Howto Integrating SAP Netveawer and OpenID http://www.sap.com/mk/get?\_EC=aVAixZ7ktAaSgYDKqVnHF6
<hr />
