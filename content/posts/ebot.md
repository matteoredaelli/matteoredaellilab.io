---
title: 'Ebot: an erlang web crawler'
date: Fri, 21 May 2010 17:43:41 +0000
draft: false
tags: ['Amqp', 'couchdb', 'Crawler', 'Database', 'Erlang', 'LinkedIn', 'Me', 'Mochiweb', 'OpenSource', 'rabbitmq', 'Web', 'Webmachine']
---

[![](http://github.com/matteoredaelli/ebot/raw/master/doc/ebot_small.png "ebot")](http://www.redaelli.org/matteo-blog/projects/ebot)[Ebot](http://www.redaelli.org/matteo-blog/projects/ebot/) is a [opensource](http://gplv3.fsf.org/) and scalable [web crawler](http://en.wikipedia.org/wiki/Web_crawler) written on top of **[Erlang](../archives/297)**, a NOSQL database ([Apache CouchDB](http://couchdb.apache.org/) or [Riak](http://riak.basho.com/)), **[RabbitMQ](http://www.rabbitmq.com/)**, [Mochiweb](http://code.google.com/p/mochiweb/), [Webmachine](http://bitbucket.org/justin/webmachine), .. See my [Erlang info](http://www.redaelli.org/matteo-blog/projects/erlang/) page for more details.
