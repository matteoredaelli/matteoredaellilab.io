---
title: 'howto extract sentences from an ebook/doc/pdf/text file'
date: Wed, 05 Apr 2017 20:43:35 +0000
draft: false
tags: ['Me']
---

![](http://tika.apache.org/tika.png) You can easily extract sentences froma epub/pdf/office/text file thanks to the opensource projects [Apache Tika](http://Apache Tika) and [Apache OpenNlp](https://opennlp.apache.org/) with a command like```
java -jar tika-app-1.14.jar -t ebook.epub | opennlp SentenceDetector it-sent.bi
```![](https://opennlp.apache.org/images/onlplogo.jpg)