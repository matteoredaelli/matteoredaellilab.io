---
title: 'ICA-ATOM (QUBIT), THE OpenSource archival description software'
date: Wed, 15 Apr 2009 08:51:30 +0000
draft: false
tags: ['Me', 'OpenSource']
---

![](http://ica-atom.org/images/ica-atom-logo-1_0.png "ICA-ATOM") I've collected some information about [ICA-ATOM](http://ica-atom.org/) (based on [QuBit](http://code.google.com/p/qubit-toolkit/)). Please read my [presentation](http://docs.google.com/Present?docID=dfr68gz6_42dz43zhdm&revision=_latest&start=0&theme=blank&cwj=true) from Google Docs