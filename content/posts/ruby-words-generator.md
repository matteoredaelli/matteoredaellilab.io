---
title: 'Ruby Wordlist Generator (ruby-wg)'
date: Mon, 13 Apr 2009 19:44:02 +0000
draft: false
tags: ['Activemq', 'OpenSource', 'Ruby']
---

![](http://ruby-words-generators.googlecode.com/svn/trunk/doc/ruby-words-generators.png "Ruby word generators") After many years I've updated my Wordlist Generator project! I've seen in several forums that some people still use my old perl script [wg.pl](http://www.redaelli.org/matteo/downloads/perl/wg.pl) The new ruby project is hosted at [htt](http://code.google.com/p/ruby-words-generators)[p://code.google.com/p/ruby-words-generators/](http://code.google.com/p/ruby-words-generators) ruby-wg can be used to generate wordlist for the famous password cracker [John the Ripper](http://www.openwall.com/john/)
