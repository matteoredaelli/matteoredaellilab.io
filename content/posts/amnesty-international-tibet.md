---
title: 'Amnesty International & Tibet'
date: Thu, 20 Mar 2008 18:28:38 +0000
draft: false
tags: ['Politica']
---

[![tibet.jpg](http://matteoredaelli.files.wordpress.com/2008/03/tibet.jpg)](http://matteoredaelli.wordpress.com/2008/03/20/amnesty-international-tibet/68/ "tibet.jpg")...a seguito delle proteste scoppiate lunedì 10 marzo in Tibet in cui sono stati arrestati numerosi monaci tibetani, Amnesty International ha lanciato oggi un'azione urgente in favore di 15 monaci arrestati e scomparsi. Si tratta di Samten, Trulku Tenpa Rigsang, Gelek Pel, Lobsang, Lobsang Thukjey, Tsultrim Palden, Lobsher, Phurden, Thupdon, Lobsang Ngodup, Lodoe, Thupwang, Pema Garwang, Tsegyam e Soepa, in carcere dal 10 marzo per aver preso parte a una manifestazione pacifica a Barkhor, Lhasa, la capitale della Regione autonoma tibetana. Non si hanno ulteriori informazioni sul luogo in cui sono detenuti né su eventuali accuse formulate nei loro confronti e Amnesty International teme rischino di subire torture e altri maltrattamenti. Perchè non firmare e far firmare l'appello per la loro scarcerazione? Valli a questa [pagina](http://www.amnesty.it/appelli/azioni_urgenti/Tibet?page=azioni_urgenti) !
