---
title: 'Connecting to a database with a webService (cxf) deployed (osgi) in Servicemix'
date: Sun, 07 Jun 2009 17:37:15 +0000
draft: false
tags: ['Me', 'Ope', 'OpenSource', 'Servicemix', 'SOA']
---

1) Install and start ServiceMix 4.0 2) Add necessary bundles

features/install cxf-osgi

osgi/install -s mvn:org.apache.servicemix.bundles/org.apache.servicemix.bundles.commons-dbcp/1.2.2\_3/

osgi/install -s wrap:mvn:org.springframework/spring-jdbc/2.5.6/

osgi/install -s wrap:mvn:mysql/mysql-connector-java/5.1.6

... See details in this [gooole document](http://docs.google.com/View?id=dfr68gz6_72684sn9vh) or download [test-wsdl-first-osgi.zip](http://www.redaelli.org/matteo/downloads/java/test-wsdl-first-osgi.zip)
---
### Comments:
#### 
[Thomas Gill]( "zzzzzz500@gmail.com") - <time datetime="2009-08-03 20:54:19">Aug 1, 2009</time>

Matteo - How do you install an Oracle driver in Servicemix. I have the oracle6.jar. Tried putting in lib dir and also install file:///tmp/ojdbc6.jar Trying to setup Master -Slave using JDBC for ActiveMQ
<hr />
#### 
[Matteo](http://www.redaelli.org "matteo.redaelli@gmail.com") - <time datetime="2009-08-03 21:03:28">Aug 1, 2009</time>

In smx4 try installing bundle from springsource http://www.springsource.com/repository/app/bundle/version/detail?name=com.springsource.oracle.jdbc&version=10.2.0.2 if you cannot, please ask in servicemix forum, I'm not an expert in java/osgi.. Regards
<hr />
#### 
[Thomas Gill]( "zzzzzz500@gmail.com") - <time datetime="2009-08-04 05:44:57">Aug 2, 2009</time>

Thanks Matteo - The Spring version is a little old but works. I have the ActiveMQ tables created in DB and everything works. I tried with one Master alone and when running it is all good. But when the Master goes down the DB lock is not removed. Planning to start 2 Brokers and see how it behaves.
<hr />
