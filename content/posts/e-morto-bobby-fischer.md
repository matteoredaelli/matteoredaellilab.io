---
title: 'E'' morto Bobby Fischer!'
date: Fri, 18 Jan 2008 22:22:17 +0000
draft: false
tags: ['Me', 'Me']
---

![](http://mychessgames.1afm.com/fischer1972u.jpg) All'eta' di 64 anni e' morto il mitico scacchista **[Bobby Fischer](http://en.wikipedia.org/wiki/Bobby_Fischer)** divenuto campione nel mondo nel 1972! Links:

> \* [http://bobbyfischer.net/](http://bobbyfischer.net/)

> \* [http://www.bobby-fischer.net/](http://www.bobby-fischer.net/)