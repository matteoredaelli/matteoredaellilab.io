---
title: 'Deploying microservices in a Docker container'
date: Fri, 11 Dec 2015 13:09:29 +0000
draft: false
tags:
- docker
- services
---

[![](http://zdnet4.cbsistatic.com/hub/i/r/2014/10/02/1f130129-49e2-11e4-b6a0-d4ae52e95e57/resize/770xauto/2598bf8706f23f291a520c42165e6b1f/docker-vm-container.png)](http://www.redaelli.org/matteo-blog/2014/08/04/docker-moving-datacentersapps-from-virtual-machines-to-containers/)I already spoke about docker containers ([moving datacenters apps from virtual machines to containers)](http://www.redaelli.org/matteo-blog/2014/08/04/docker-moving-datacentersapps-from-virtual-machines-to-containers/) This is a quick tutorial (my [github sample code](https://github.com/matteoredaelli/python-ws-docker)) about a new way of deploying (micro) services and applications, ie using [Docker](https://www.docker.com/) containers: a sample python webservice and an simple web (html + [angularJS](https://angularjs.org/) code) page Creating docker containers means defining a file Dockerfile like```
FROM python:3.5
#FROM python:3-onbuild

ENV DEBIAN\_FRONTEND noninteractive

ENV HTTP\_PROXY="http://myproxy.redaelli.org:80"
ENV HTTPS\_PROXY="http://myproxy.redaelli.org:80"
ENV http\_proxy="http://myproxy.redaelli.org:80"
ENV https\_proxy="http://myproxy.redaelli.org:80"
ENV PIP\_OPTIONS="--proxy $HTTP\_PROXY"

COPY requirements.txt /usr/src/app/
COPY app.py /usr/src/app/

WORKDIR /usr/src/app
RUN apt-get update && apt-get install -y nmap
RUN pip install --proxy $HTTP\_PROXY --no-cache-dir -r requirements.txt

VOLUME \["/usr/src/app"\]
EXPOSE 5000

ENTRYPOINT \["python"\]
CMD \["./app.py"\]


```Put the additional python packages you need in a file requirements.txt```
Flask
python-nmap
dnspython3
```And create your application in the file app.py In this way we are going to create a docker container with python3 and some additional python packages with the command```
docker build -t python-infra-ws .
```Finally we'll start the container with the command```
docker run -d -t --name python-infra-ws -p 5000:5000 python-infra-ws
```Some other useful commands are:```
docker stop python-infra-ws
docker start python-infra-ws
docker ps python-infra-ws
docker rm python-infra-ws
docker rmi python-infra-ws
```
