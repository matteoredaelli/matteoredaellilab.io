---
title: 'ATM: collegamento Linate con le stazioni FS o fermate della metropolitana'
date: Mon, 25 Feb 2008 09:51:39 +0000
draft: false
tags: ['Trasporti', 'Uncategorized']
---

ATM, nella sua pagina dedicata sul quotidiano gratuito City, oggi ha fatto pubblicita' all'UTILE servizio di collegamento tra Linate e San Babila fornito dalla linea 73. La considero una presa in giro e un'offesa per tutti i cittadini milanesi e lombardi! E una concessione alla lobby dei tassisti... La maggior parte di chi parte/arriva a Linate abita in centro a Milano? Oppure quanti hanno la NECESSITA' di passare per il centro della citta' con le valigie prima o dopo il volo? Quello di cui abbiamo bisogno e' un collegamento tra Linate e le stazioni FS (tipo Lambrate e Rogoredo) e del metro' (tipo Gobba) della citta'. Vergogna!!!!!!!!