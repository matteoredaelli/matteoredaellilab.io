---
title: 'Building websites for high traffic with REST APIs, AngularJs and Jekyll'
date: Thu, 05 May 2016 21:19:57 +0000
draft: false
tags:
- angularjs
- jekyll
- rest
---

If you have few hw resources and/or you expect high traffic on your website, here are some quick suggestions (also taken from the article [Meet the Obama campaign's $250 million fundraising platform](http://kylerush.net/blog/meet-the-obama-campaigns-250-million-fundraising-platform/)):

*   Expose your business logic with REST services
*   Use a javascript framework like angularJS for calling your rest APIs and building a dynamic site
*   Build a (not so) static website using Jekyll or similars and put your static files on S3 (if you are using Amazon AWS)
*   use a CDN

A sample website (not hosted at AWS but home based using a poor raspberryPI2) is [http://paroleonline.it](http://paroleonline.it)
