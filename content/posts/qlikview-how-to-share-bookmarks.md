---
title: 'QlikView: how to share bookmarks'
date: Tue, 10 Mar 2015 16:07:28 +0000
draft: false
tags:
- qlikview
---

In your [QlikView](http://www.qlik.com) report, select the "Repository" button (the first from the right) [![qlik-bookmarks-1](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/03/qlik-bookmarks-1.jpg)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/03/qlik-bookmarks-1.jpg) and then select "Bookmarks"... [![qlik-bookmarks-2](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/03/qlik-bookmarks-2.jpg)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/03/qlik-bookmarks-2.jpg)
