---
title: 'Before SQL then NOSQL and BIGDATA: now BIGDATA and SQL again'
date: Mon, 02 Mar 2015 13:37:35 +0000
draft: false
tags:
- bigdata
- sql
- nosql
- prestodb
- drill
- apache
---

[![drill](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/03/drill.jpg)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/03/drill.jpg) ![](https://spark.apache.org/images/spark-logo.png) The trend of these years has been switching from SQL (RDBMS) databases to NoSQL databases like Hadoop, MongoDB, Cassandra, Riak, ... ![](http://phoenix.apache.org/images/phoenix-logo-small.png) SQL is a old but easy and fast way to query data. And people STILL look at it for quering Hadoop and bigdata:

*   [Apache Drill](http://drill.apache.org/)  (MapR)
*   [Apache Phoenix](http://phoenix.apache.org/)
*   [Apache Hive](https://hive.apache.org/) (see [Stinger initiative](http://hortonworks.com/labs/stinger/))
*   [Apache Spark SQL](https://spark.apache.org/sql/)
*   [Presto](https://prestodb.io/) (Facebook)
*   [Apache Tajo](http://tajo.apache.org/) (a datawarehouse)
*   [Impala (Cloudera)](http://www.cloudera.com/content/cloudera/en/products-and-services/cdh/impala.html)

![](https://hive.apache.org/images/hive_logo_medium.jpg) ![](http://drill.apache.org/images/arc-1.jpg) Read details from [10 ways to query hadoop with sql](http://www.infoworld.com/article/2683729/hadoop/10-ways-to-query-hadoop-with-sql.html) .. ![](https://prestodb.io/static/presto-overview.png)
