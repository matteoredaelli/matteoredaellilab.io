---
title: 'Sample microservice for exposing database tables via REST using node.js express.js in a docker container'
date: Thu, 09 Feb 2017 17:17:27 +0000
draft: false
tags: ['nodejs', 'services', 'database']
---

![](http://thisdavej.com/wp-content/uploads/2016/02/nodejs-logo.png)I have started learning [node.js](https://nodejs.org/): The first result is this [github repository](https://github.com/matteoredaelli/node-express-rest-oracle-docker) where you can find a basic project of a rest web service that exposes oracle data. How to run the service forever (with autorestart on failures)? I tested it both with a docker container and the [pm2](http://pm2.keymetrics.io/) tool ![](http://zdnet4.cbsistatic.com/hub/i/r/2014/10/02/1f130129-49e2-11e4-b6a0-d4ae52e95e57/resize/770xauto/2598bf8706f23f291a520c42165e6b1f/docker-vm-container.png)
