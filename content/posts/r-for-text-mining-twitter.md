---
title: 'R for text mining & twitter'
date: Wed, 12 Jun 2013 20:35:49 +0000
draft: false
tags: ['R']
---

![](https://pbs.twimg.com/media/BMkSHVvCcAAsitU.png:medium)This is a sample report of topics of tweets related to

[![ddaypirelli-who-retweets-whom2](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/ddaypirelli-who-retweets-whom2.png)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/ddaypirelli-who-retweets-whom2.png)

#ddaypirelli  as suggested by [miningtwitter](https://sites.google.com/site/miningtwitter/) website using the [R statistical](http://www.r-project.org/) environment and some of its [thousands libraries](http://cran.r-project.org/)

![](https://pbs.twimg.com/media/BMj33OpCEAAoOcT.png:medium)
