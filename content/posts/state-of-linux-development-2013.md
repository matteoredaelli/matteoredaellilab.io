---
title: 'State of Linux Development 2013'
date: Thu, 21 Nov 2013 15:56:10 +0000
draft: false
tags: ['linux']
---

![](https://pbs.twimg.com/media/BZi-fN3CQAAOmqG.png:large)

From twitter @linuxfondation account

10,000 developers from 1,000 companies have contributed to [#**Linux**](https://twitter.com/search?q=%23Linux&src=hash) since 2005.

Are you one of them? [pic.twitter.com/6BIUgKDhnM](http://t.co/6BIUgKDhnM)
