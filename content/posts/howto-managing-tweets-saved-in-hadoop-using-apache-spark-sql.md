---
title: 'Howto managing tweets saved in #Hadoop using #Apache #Spark SQL'
date: Thu, 15 Jan 2015 16:35:40 +0000
draft: false
tags: ['Me']
---

Instead of using the old [Hadoop](http://hadoop.apache.org/) way (map/reduce), I suggest using the newer and faster way ([Apache Spark](https://spark.apache.org/) on top of Hadoop Yarn): in few lines you can open all tweets (zipped json files saved in several subdirectories hdfs://path/to/YEAR/MONTH/DAY/\*gz) and query them in a SQL like language```
sc = SparkContext(appName="extraxtStatsFromTweets.py")
sqlContext = SQLContext(sc)
tweets = sqlContext.jsonFile("/tmp/twitter/opensource/2014/\*/\*.gz")
tweets.registerTempTable("tweets") 
t = sqlContext.sql("SELECT distinct createdAt,user.screenName,hashtagEntities FROM tweets")
tweets\_by\_days = count\_items(t.map(lambda t: javaTimestampToString(t\[0\])))
stats\_hashtags = count\_items(t.flatMap(lambda t: t\[2\])\\ .map(lambda t: t\[2\].lower()))
```My source codes are free and available ([Python](https://github.com/matteoredaelli/extractTweets.py) and [Scala](https://github.com/matteoredaelli/extractTweets) repositories). ![](https://raw.githubusercontent.com/matteoredaelli/extractTweets.py/master/doc/opensource-1.jpg)   ![](https://raw.githubusercontent.com/matteoredaelli/extractTweets.py/master/doc/opensource-2.jpg)