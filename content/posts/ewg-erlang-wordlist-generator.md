---
title: 'Ewg: Erlang Wordlist Generator'
date: Tue, 04 Aug 2009 20:01:10 +0000
draft: false
tags: ['Erlang', 'Me', 'OpenSource']
---

[![logo](http://matteoredaelli.files.wordpress.com/2009/08/logo.png "logo")](http://github.com/matteoredaelli/ewg/) [Ewg](http://github.com/matteoredaelli/ewg/) is a Wordlist generator written in Erlang. Thanks to this simple project I have been improving my knowledge in Erlang/OTP and Git. I love functional programming and learning more and more... ;-)