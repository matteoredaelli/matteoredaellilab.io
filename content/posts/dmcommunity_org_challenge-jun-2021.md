---
title: Where is gold challenge with Prolog
date: 2021-06-02
draft: false
tags:
- prolog
- programming
---

My #prolog solution for "Where is gold?" proposed by [dmcommunity.org challenge Jun 2021](https://dmcommunity.org/challenge/challenge-june-2021/): 0 means empty, 1 means gold

``` prolog
?- solution(Box1,Box2,Box3).
Box1 = Box3, Box3 = 0,
Box2 = 1.
```

below the code

``` prolog
:-use_module(library(clpfd)).

sentence1(    1,_Box2,_Box3).
not_sentence1(0,_Box2,_Box3).

sentence2(    _Box1,0,_Box3).
not_sentence2(_Box1,1,_Box3).

sentence3(    0,_Box2,_Box3).
not_sentence3(1,_Box2,_Box3).

true_only_one_sentence(Box1, Box2, Box3):-
	(       sentence1(Box1,Box2,Box3), not_sentence2(Box1,Box2,Box3), not_sentence3(Box1,Box2,Box3) ) ;
	(   not_sentence1(Box1,Box2,Box3),     sentence2(Box1,Box2,Box3), not_sentence3(Box1,Box2,Box3) ) ;
	(   not_sentence1(Box1,Box2,Box3), not_sentence2(Box1,Box2,Box3),     sentence3(Box1,Box2,Box3) ).

solution(Box1, Box2, Box3):-
	Box1 in 0..1, /* 0 empty, 1 gold */
	Box2 in 0..1,
	Box3 in 0..1,
	Box1 + Box2 + Box3 #= 1, /* only one box contains gold */
	true_only_one_sentence(Box1, Box2, Box3).
```
