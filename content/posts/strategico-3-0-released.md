---
title: 'Strategico 3.0 released!'
date: Tue, 25 Sep 2012 21:00:32 +0000
draft: false
tags: ['OpenSource', 'R', 'statistics']
---

[![](http://strategico.googlecode.com/svn/trunk/doc/strategico-stats.png "forecasting prediction")](http://strategico.googlecode.com)

[![](http://strategico.googlecode.com/svn/trunk/doc/strategico2.png "Strategico Long Term Prediction")](http://code.google.com/p/strategico/)

[Strategico](http://code.google.com/p/strategico/) 3.0 has been released!

**Strategico** is an opensource tool for  making **forecasts and Long Term Predictions ** over a (huge) set of **time series**

Strategico is written with [R](http://www.r-project.org/), the most famous and used  Statistical programming language
