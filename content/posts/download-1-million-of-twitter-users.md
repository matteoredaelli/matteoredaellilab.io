---
title: 'Download 1 million of twitter users'
date: Mon, 07 Dec 2015 17:06:54 +0000
draft: false
tags: ['Me']
---

![](http://dailygenius.com/wp-content/uploads/2014/09/twittercloud.png)I like learning and using new technologies and opensource softwares. For instance I use [Apache Camel](http://camel.apache.org/) and [MongoDB](https://www.mongodb.org/) for downloading and analyzing twitter data.  [Here](http://www.redaelli.org/matteo/downloads/twitter-users/tu1.zip) you can download a sample file with (some attributes of) 1 million of twitter users