---
title: 'Continuous integration and continuous delivery with Jenkins'
date: Fri, 10 Mar 2017 14:25:45 +0000
draft: false
tags:
- jenkins
- CI/CD
- opensource
---

![](http://www.redaelli.org/matteo-blog/wp-content/uploads/2017/03/jenkins-obiee.png) In this post I'll show how to use the opensource tool #[Jenkins](https://jenkins.io/), "the leading #opensource automation server, Jenkins provides hundreds of plugins to support building, deploying and automating any project". I'll create a simple pipeline that executes remote tasks via ssh. It could be used for _continuous integration_ and continuous delivery for Oracle OBIEE Systems

#### Install (in a docker container)

```
docker run -p 8080:8080 -p 50000:50000 -v /home/oracle/docker\_shares/jenkins:/var/jenkins\_home -d jenkins
```

#### Configure credentials

Login to Jenkins  (http://jenkins.redaelli.org:8080) Jenkins -> Manage Jenkins -> Credential -> System -> Add credential

#### Configure remote nodes

Jenkins -> Manage Jenkins -> Manage nodes ->  Add node

#### ![](http://www.redaelli.org/matteo-blog/wp-content/uploads/2017/03/jenkins-ssh-node.png)

Configure Pipeline Jenkins -> New Item -> Pipeline See [https://gist.github.com/matteoredaelli/8d306d79e547f3fdfd5d1c467373f8e0](https://gist.github.com/matteoredaelli/8d306d79e547f3fdfd5d1c467373f8e0) ![](https://jenkins.io/images/226px-Jenkins_logo.svg.png)
