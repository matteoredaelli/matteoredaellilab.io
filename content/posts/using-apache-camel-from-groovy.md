---
title: 'Using Apache Camel from Groovy'
date: Thu, 03 Oct 2019 13:22:27 +0000
draft: false
tags:
- apache
- camel
- groovy
---

[Apache Camel](https://camel.apache.org/) is an open source integration framework that empowers you to quickly and easily integrate various systems consuming or producing data.

[Apache](https://en.wikipedia.org/wiki/Apache_Software_Foundation) **Groovy** is a Java-syntax-compatible [object-oriented](https://en.wikipedia.org/wiki/Object-oriented_programming)[programming language](https://en.wikipedia.org/wiki/Programming_language) for the [Java platform](https://en.wikipedia.org/wiki/Java_(software_platform)). It is both a static and [dynamic](https://en.wikipedia.org/wiki/Dynamic_programming_language) language with features similar to those of [Python](https://en.wikipedia.org/wiki/Python_(programming_language)), [Ruby](https://en.wikipedia.org/wiki/Ruby_(programming_language)), and [Smalltalk](https://en.wikipedia.org/wiki/Smalltalk). It can be used as both a [programming language](https://en.wikipedia.org/wiki/Programming_language) and a [scripting language](https://en.wikipedia.org/wiki/Scripting_language) for the Java Platform, is compiled to [Java virtual machine](https://en.wikipedia.org/wiki/Java_virtual_machine) (JVM) [bytecode](https://en.wikipedia.org/wiki/Bytecode), and interoperates seamlessly with other Java code and [libraries](https://en.wikipedia.org/wiki/Library_(computing)). Groovy uses a [curly-bracket syntax](https://en.wikipedia.org/wiki/Curly_bracket_programming_language) similar to Java's. Groovy supports [closures](https://en.wikipedia.org/wiki/Closure_(computer_programming)), multiline strings, and [expressions embedded in strings](https://en.wikipedia.org/wiki/String_interpolation). Much of Groovy's power lies in its [AST](https://en.wikipedia.org/wiki/Abstract_syntax_tree) transformations, triggered through annotations. \[Wikipedia\]

```
Create a file camel-test.groovy like the following

 @Grab('org.apache.camel:camel-core:2.21.5')
 @Grab('javax.xml.bind:jaxb-api:2.3.0')
 @Grab('org.slf4j:slf4j-simple:1.7.21')
 @Grab('javax.activation:activation:1.1.1')

 import org.apache.camel.\*
 import org.apache.camel.impl.\*
 import org.apache.camel.builder.\*
 def camelContext = new DefaultCamelContext()
 camelContext.addRoutes(new RouteBuilder() {
	 def void configure() {
		 from("timer://jdkTimer?period=3000")
			 .to("log://camelLogger?level=INFO")
	 }
 })
 camelContext.start()
 addShutdownHook{ camelContext.stop() }
 synchronized(this){ this.wait() }
```

Test it with

JAVA\_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64 groovy camel-test.groovy
