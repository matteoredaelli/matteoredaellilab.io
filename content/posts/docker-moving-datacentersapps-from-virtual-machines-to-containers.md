---
title: 'Docker: moving datacenters/apps from Virtual Machines to containers'
date: Mon, 04 Aug 2014 19:02:23 +0000
draft: false
tags: ['docker', 'Me']
---

[![](http://cdn-static.zdnet.com/i/r/story/70/00/032269/docker-vm-container-620x350.png?hash=AmVjAGDlAG&upscale=1)](http://www.zdnet.com/what-is-docker-and-why-is-it-so-darn-popular-7000032269/)

["Docker](https://www.docker.com/), a new container technology, is hotter than hot because it makes it possible to get far more apps running on the same old servers and it also makes it very easy to package and ship programs..." Read the full article at [zdnet.com](http://www.zdnet.com/what-is-docker-and-why-is-it-so-darn-popular-7000032269/)