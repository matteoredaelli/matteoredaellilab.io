---
title: 'Detassiamo l''abbonamento ai mezzi pubblici!'
date: Fri, 03 Aug 2007 19:00:03 +0000
draft: false
tags: ['Ambiente', 'Uncategorized']
---

Lavoro per un noto gruppo industriale a Milano, citta' decadente a causa del traffico, dell'aria inquinata e l'immobilita' politico ambientale degli ultimi decenni. Conosco diversi colleghi che pur vivendo in citta' vengono sistematicamente al lavoro ogni giorno in macchina. Come si potrebbe incentivare i cittadini e pendolari all'utilizzo dei mezzi pubblici? Oltre alla scontato miglioramento della frequenza dei mezzi si potrebbe permettere ai lavoratori di detrarre dalle tasse il costo degli abbonamenti ATM e Trenitalia usati per recarsi al lavoro. Che ne dite? VI sembra un'idea troppo insensata o irrealizzabile?!?!?!
---
### Comments:
#### 
[superbolo](http://www.superbolo.altervista.org/wp/ "stefano_bolognani@hotmail.com") - <time datetime="2007-08-14 13:44:39">Aug 2, 2007</time>

I mezzi di trasporto in Italia sono ben lontani dall'essere efficenti, puliti e moderni. E quando lo sono, per colpa dei soliti, e quasi impossibile utilizzarli. E poi ci sono gli orari, quasi sempre studiati da chi non ha reale necessità di prenderli. Certo, è facile fare critica, ma purtroppo è anche troppo difficile ottenere qualcosa. E anche per chi utilizza le due ruote per recarsi al lavoro, non è facile e non incentivato. Ecco quindi che tanti preferiscono rintanarsi per ore ed ore in una scatola in coda, pero' al fresco dell'aria condizionata od al caldo del riscaldamento. Che vita pero' ....
<hr />
#### 
[laboratoriomobilitasostenibile](http://laboratoriomobilitasostenibile.wordpress.com/ "mobilitasostenibile@gmail.com") - <time datetime="2007-11-29 18:26:40">Nov 4, 2007</time>

... a volte arriva qualche buona notizie (anche se c'è da vedere se passerà davvero...). come questa, contenuta nella finanziaria 2008: Trasporto pubblico locale Per le spese, non superiori a 250 euro, sostenute fino al 31 dicembre 2008 per l'acquisto di abbonamenti ai servizi di trasporto pubblico locale, regionale e interregionale, è prevista una detrazione del 19 per cento. Per poterne usufruirne è necessario che le spese non siano deducibili dai singoli redditi che concorrono a formare quello complessivo. chissà! speriamo :)
<hr />
