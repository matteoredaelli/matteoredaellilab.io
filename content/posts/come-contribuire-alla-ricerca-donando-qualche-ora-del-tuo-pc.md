---
title: 'Come contribuire alla ricerca donando qualche ora del tuo PC'
date: Wed, 19 Nov 2008 22:15:54 +0000
draft: false
tags: ['Me', 'OpenSource', 'Ricerca']
---

E' facile. Scarica un piccolo programma ([boinc](http://boinc.berkeley.edu/)) e installalo sul tuo computer. Nei tempi morti o comunque quando vuoi tu, il programma si collega ai centri di ricerca, scarica un job da elaborare e quando ha terminato reinvia i risultati scaicando poi un nuovo job.... [World Community Grid](http://www.worldcommunitygrid.org/) e'  un esempio e si occupa di progetti "that hold tremendous potential to benefit humanity.

*   Nutritious Rice for the World
*   Help Conquer Cancer
*   Discovering Dengue Drugs - Together
*   Human Proteome Folding - Phase 2 Project
*   FightAIDS@Home Project
*   How Grid Computing Works

Io personalmente mi sono iscritto al [Team di Oses](http://osesassociazione.wordpress.com/2008/11/09/world-community-grid-diamo-un-contributo/).