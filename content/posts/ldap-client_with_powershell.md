---
title: 'Active Directory client with powershell'
date: Fri, 19 Apr 2019 07:37:24 +0000
draft: false
tags:
- powershell
- ldap
- active directory
- csv
---

# Add AD users from csv to group using powershell

A sample script for adding users (taken from a csv file) to an Active Directory group
```
$GroupName = "Qliksense_SI_Techedge"
$Users =  "e:\scripts\users.csv"

Import-module ActiveDirectory

$dc = Get-ADDomainController -DomainName mydomain.redaelli.org -Discover -NextClosestSite
$server = $dc.HostName[0]

get-content $Users | ForEach-Object {
  Get-ADUser -Server $server -LDAPFilter "(mail=$_)" } |
  Select-Object -ExpandProperty sAMAccountName |  ForEach-Object { Add-ADGroupMember -Server $server -Identity $GroupName -Member $_ }
```
