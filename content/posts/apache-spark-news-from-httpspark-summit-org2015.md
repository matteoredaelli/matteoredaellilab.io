---
title: 'Apache Spark news from a Spark Summit 2015'
date: Tue, 21 Apr 2015 20:12:00 +0000
draft: false
tags:
- apache
- spark
---

[![spark-2015-from-databricks-pdf](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/04/spark-2015-from-databricks-pdf.png)](http://spark-summit.org/wp-content/uploads/2015/03/SSE15-1-Matei-Zaharia.pdf)

**GOAL: unified engine across data sources, workloads and environments.**

Highlights: dataframes (1.3), SparkR (1.4), ...

See all video and slides at [http://spark-summit.org](http://spark-summit.org)
