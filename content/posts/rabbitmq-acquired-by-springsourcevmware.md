---
title: 'RabbitMQ acquired by SpringSource/VMWare'
date: Tue, 13 Apr 2010 18:48:37 +0000
draft: false
tags: ['Erlang', 'LinkedIn', 'OpenSource']
---

![](http://www.rabbitmq.com/img/rabbitmqlogonostrap.png "rabbitmq")"Erlang-based [RabbitMQ](http://www.rabbitmq.com/) was acquired today by [SpringSource](http://www.springsource.com/), which is owned by [VMWare](http://www.vmware.com)" "RabbitMQ is a complete and highly reliable enterprise messaging system based on the [emerging AMQP standard](http://www.rabbitmq.com/specification.html). It is licensed under the open source Mozilla Public License and has a platform-neutral distribution, plus platform-specific packages and bundles for easy installation." Sponsors of [**AMQP**](http://www.amqp.org) are: Bank of America, N.A., Barclays Bank PLC, Cisco Systems, Credit Suisse, Deutsche Börse Systems, Envoy Technologies Inc., Goldman Sachs, iMatix Corporation sprl., INETCO Systems Limited, JPMorgan Chase Bank Inc. N.A, Microsoft Corporation, Novell, Progress Software, Rabbit Technologies Ltd., Red Hat Inc., Solace Systems Inc., Tervela Inc., TWIST Process Innovations Ltd, WS02 Inc. and 29West Inc... (see [here](http://www.amqp.org/confluence/display/AMQP/AMQP+Working+Group))![](http://www.amqp.org/confluence/download/attachments/3178497/global.logo?version=1&modificationDate=1260485198000 "amqp") Go [here](http://www.springsource.com/newsevents/springsource-acquires-rabbitmq-cloud-messaging) or [here](http://erlanginside.com/rabbitmq-acquired-springsource-vmware-162) for more details...
