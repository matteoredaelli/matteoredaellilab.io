---
title: 'SAP Contributes New Tool to Open Source Eclipse Developer Community'
date: Tue, 06 Nov 2007 20:04:39 +0000
draft: false
tags:
- java
- OpenSource
- sap
---

_Advanced Memory Analyzer Capabilities Enable Eclipse Developers to Build Enterprise Applications Within Leading Open Source Development Environment_ Read the rest f the article at [CNN](http://money.cnn.com/news/newsfeeds/articles/prnewswire/AQW04517102007-1.htm). Sap Wiki at [https://wiki.sdn.sap.com/wiki/x/k2w](https://wiki.sdn.sap.com/wiki/x/k2w)
