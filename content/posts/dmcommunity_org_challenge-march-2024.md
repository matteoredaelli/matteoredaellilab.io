---
title: 'DM Community - challenge March 2024 - generative ai'
date: 2024-03-28
draft: false
tags: ['ai', 'claude', 'chatgpt', 'gemini']
---

Usually I solve DM Community challenges using the Prolog language. Regarding https://dmcommunity.org/challenge/challenge-march-2024/ I used the generative AI. 


Below the prompt I used but the results are not always good!
What's wrong? How could I change my prompt in order to get better results?

```
Hello. 
Act as developer. 
Given a list of "employees" in the following json format: '{
  "company" : {
    "companyName" : "ABC",
    "employees" : [ {
      "name" : "Robinson",
      "age" : 25,
      "gender" : "Female",
      "maritalStatus" : "Married",
      "minor" : false,
      "locations" : [ {
        "id" : "RobinsonLoc1",
        "street" : "Main Str",
        "zipCode" : "08831",
        "state" : "NJ"
      }, {
        "id" : "RobinsonLoc2",
        "street" : "Ocean Drive",
        "zipCode" : "33019",
        "state" : "FL"
      } ],
      "children" : 2,
      "salary" : 220000.0
    }, {
      "name" : "Warner",
      "age" : 45,
      "gender" : "Male",
      "maritalStatus" : "Married",
      "minor" : false,
      "locations" : [ {
        "id" : "WarnerLoc",
        "street" : "Maple Street",
        "zipCode" : "08817",
        "state" : "NJ"
      } ],
      "children" : 0,
      "salary" : 150000.0
    }, {
      "name" : "Stevens",
      "age" : 24,
      "gender" : "Male",
      "maritalStatus" : "Single",
      "minor" : false,
      "locations" : [ {
        "id" : "StevensLoc",
        "street" : "Regency Drive",
        "zipCode" : "08817",
        "state" : "NJ"
      } ],
      "children" : 0,
      "salary" : 135000.0
    }, {
      "name" : "White",
      "age" : 32,
      "gender" : "Female",
      "maritalStatus" : "Married",
      "minor" : false,
      "locations" : [ {
        "id" : "WhiteLoc",
        "street" : "Green Road",
        "zipCode" : "33019",
        "state" : "FL"
      } ],
      "children" : 2,
      "salary" : 195000.0
    }, {
      "name" : "Smith",
      "age" : 46,
      "gender" : "Male",
      "maritalStatus" : "Single",
      "minor" : false,
      "locations" : [ {
        "id" : "SmithLoc",
        "street" : "Maple Street",
        "zipCode" : "90027",
        "state" : "CA"
      } ],
      "children" : 1,
      "salary" : 120000.0
    }, {
      "name" : "Green",
      "age" : 28,
      "gender" : "Female",
      "maritalStatus" : "Married",
      "minor" : false,
      "locations" : [ {
        "id" : "GreenLoc",
        "street" : "Green Road",
        "zipCode" : "33019",
        "state" : "FL"
      } ],
      "children" : 3,
      "salary" : 140000.0
    }, {
      "name" : "Brown",
      "age" : 32,
      "gender" : "Male",
      "maritalStatus" : "Married",
      "minor" : false,
      "locations" : [ {
        "id" : "BrownLoc",
        "street" : "Green Road",
        "zipCode" : "33019",
        "state" : "FL"
      } ],
      "children" : 2,
      "salary" : 65000.0
    }, {
      "name" : "Klaus",
      "age" : 54,
      "gender" : "Male",
      "maritalStatus" : "Married",
      "minor" : false,
      "locations" : [ {
        "id" : "KlausLoc",
        "street" : "Green Road",
        "zipCode" : "33019",
        "state" : "FL"
      } ],
      "children" : 1,
      "salary" : 85000.0
    }, {
      "name" : "Houston",
      "age" : 47,
      "gender" : "Female",
      "maritalStatus" : "Single",
      "minor" : false,
      "locations" : [ {
        "id" : "HoustonLoc",
        "street" : "Green Road",
        "zipCode" : "33019",
        "state" : "FL"
      } ],
      "children" : 2,
      "salary" : 135000.0
    }, {
      "name" : "Long",
      "age" : 29,
      "gender" : "Male",
      "maritalStatus" : "Married",
      "minor" : false,
      "locations" : [ {
        "id" : "LongLoc",
        "street" : "Green Road",
        "zipCode" : "33019",
        "state" : "FL"
      } ],
      "children" : 3,
      "salary" : 40000.0
    }, {
      "name" : "Short",
      "age" : 22,
      "gender" : "Male",
      "maritalStatus" : "Single",
      "minor" : false,
      "locations" : [ {
        "id" : "ShortLoc",
        "street" : "Green Road",
        "zipCode" : "33019",
        "state" : "FL"
      } ],
      "children" : 0,
      "salary" : 120000.0
    }, {
      "name" : "Doe",
      "age" : 21,
      "gender" : "Female",
      "maritalStatus" : "Single",
      "minor" : false,
      "locations" : [ {
        "id" : "DoeLoc1",
        "street" : "Green Road",
        "zipCode" : "33019",
        "state" : "FL"
      }, {
        "id" : "DoeLoc2",
        "street" : "Morgan Street",
        "zipCode" : "33020",
        "state" : "FL"
      }, {
        "id" : "DoeLoc3",
        "street" : "Lyric Ave",
        "zipCode" : "90027",
        "state" : "CA"
      } ],
      "children" : 1,
      "salary" : 210000.0
    } ],
    "selectedZipCode" : "08817"
  }
}'

please answer to the following questions:

1) What is the current total number of employees?
2a) How many children all employees have? 
2b) How many children does the average employee have?
3a) What is an average salary? 
3b) What is the maximal and minimal salaries?
4) How many employees are single?
5) In which states do the employee with "name" "Warner" have residences?
6a) How many people are inside 20% of highest paid employees? 
6b) Who are these high-paid employees?
7) List employees living in zipCode "33019".
```


## Claude (https://claude.ai/)

```
1) The current total number of employees is 12.
2a) All employees have a total of 16 children
2b) The average employee has 1.33 children
3a) The average salary is $135416.67
3b) The maximum salary is $220,000.0 and the minimum salary is $40,000.0.
4) There are 4 single employees
5)The employee 'Warner' has residences in the following states: NJ
6a) There are 2 people in the top 20% of salaries
6b)The highest paid employees are:
 - Robinson ($220000.00)
 - Doe ($210000.00)
7) Employees living in zipcode 33019:
 - Robinson
 - White
 - Green
 - Brown
 - Klaus
 - Houston
 - Long
 - Short
 - Doe
```

## ChatGPT (https://chat.openai.com/)

From the previous prompt I removed "without giving the source code"

```
1) Total number of employees: 12
2a) Total number of children all employees have: 16
2b) Average number of children per employee: 1.3333333333333333
3a) Average salary: 140416.66666666666
3b) Maximal salary: 220000.0
    Minimal salary: 40000.0
4) Number of single employees: 4
5) States where Warner has residences: ['NJ']
6a) Number of people inside 20% of highest-paid employees: 5
6b) High-paid employees: ['Robinson', 'Doe', 'White', 'Warner', 'Houston']
7) Employees living in zipCode 33019: ['Robinson', 'White', 'Green', 'Brown', 'Klaus', 'Houston', 'Long', 'Short', 'Doe']
```

## Gemini (https://gemini.google.com)

From the previous prompt I removed "without giving the source code"

```
1) Total Employees:  There are 12 employees in the company.
2a) Total Children:  All employees have a total of 18 children (2+0+0+2+1+3+2+1+2+3+0+1).
2b) Average Children:  There's an average of 1.5 children per employee (18 children / 12 employees).
3a) Average Salary:  The average salary is $141,666.67 (sum of all salaries / number of employees).
3b) Min & Max Salary:  The maximum salary is $220,000 (Robinson) and the minimum salary is $40,000 (Long).
4) There are 4 single employees (Stevens, Smith, Houston, Short).
5) Warner has a residence only in New Jersey (NJ) based on the "state" field in his location details.
6a) Number:  The top 2 employees (20% of 12) are in the highest salary range.
6b) Employees: The highest-paid employees are Robinson ($220,000) and Doe ($210,000).
7) Employees in Zip Code "33019":  These employees live in zip code "33019":
    White
    Green
    Brown
    Klaus
    Houston
    Long
    Doe (one of their residences)
```

## My python code

```python
emps = json_data['company']['employees']
# 1) 
len(emps)  12
# 2a
sum(map(lambda x: x['children'], emps)) # 17
# 2b)
sum(map(lambda x: x['children'], emps)) / 12 # 1.416
# 3a)
sum(map(lambda x: x['salary'], emps)) / 12.0 # 134583.3
# 3b)
max(map(lambda x: x['salary'], emps)) # 220000.0
min(map(lambda x: x['salary'], emps)) # 40000.0
# 4)
len(list(filter(lambda x: x['maritalStatus'] == 'Single', emps)))
# 5)
list(filter(lambda x: x['name'] == 'Warner', emps))[0]['locations'] # [{'id': 'WarnerLoc', 'street': 'Maple Street', 'zipCode': '08817', 'state': 'NJ'}]
# 6b)
list(map(lambda x: x['name'], sorted(emps, key=lambda x: x['salary'], reverse=True)[0:3])) # ['Robinson', 'Doe', 'White']
