---
title: 'Do NOT BUY any DVB USB devices with TRIDENT chipsets'
date: Fri, 01 Jan 2010 20:04:18 +0000
draft: false
tags: ['dvb', 'internet', 'Linux', 'Me', 'OpenSource', 'trident']
---

![](http://www.freecom.com/objects/00007378.jpg "dvb freecom ")Please do **NOT buy** _any DVB USB_ devices with [**TRIDENT**](http://www.tridentmicro.com "TRIDENT") chipsets like [Freecom DVB-T & Analog TV USB STICK](http://www.freecom.com/ecproduct_detail.asp?ID=2823 "DVB-T & Analog TV USB STICK") (USB Id 14aa:0620), Moka Hybrid TV receiver MK-DVBT-DUAL (USB Id 14aa:0620), LifeView LR535 ... a PCIe Mini Card (USB Id 10fd:0535), ADS Tech Mini Dual TV (PTV-339) (USB ID 06e1:b339) If you are a **Linux** User, please share this news in INTERNET: it is about  **[user generated content](http://en.wikipedia.org/wiki/User-generated_content "User Generated Content (UGC)")** AGAINST TRIDENT  and other CLOSED source vendors!!! "It is important to notice that the vendor (Trident) doesn't seem to want helping with open source development. Contacts with the vendor were tried during 2007 and 2008 in order to get their help by opening docs, via Linux Foundation NDA program, without success \[...\] In brief, while we want to fix the driver issues, it is recommend to avoid buying any devices with tm5600/tm6000/tm6010 (and DRX demod) chips.". See [http://www.linuxtv.org/wiki/index.php/Trident\_TM6000](http://www.linuxtv.org/wiki/index.php/Trident_TM6000 "http://www.linuxtv.org/wiki/index.php/Trident_TM6000") for details.
---
### Comments:
#### 
[Lincoln B. Marcelo]( "buenoview@ig.com.br") - <time datetime="2011-01-02 05:01:07">Jan 0, 2011</time>

Of course I want sources to make my own Delphi/Kylix program to control my tv adaptor box. It has tm5600 and I want to write my own controller. I bought a ENUTV-2 USB TV box this month (Dec 2010) and I think I have rights as user and owner of a device that has this chip inside to write computer controls to use it as i want. I need an linux driver to make it work under SuSE, I need the commands to make my Pascal or C++ personal controller for it.
<hr />
