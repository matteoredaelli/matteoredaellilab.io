---
title: 'Linux in the slum'
date: Sun, 11 Nov 2007 07:59:48 +0000
draft: false
tags: ['Africa', 'No profit', 'Uncategorized', 'Volontariato']
---

[AfrikaSi](http://www.afrikasi.org/) ha completato la prima fase del suo progetto pilota "Linux in the slum", iniziativa volta a promuovere l'alfabetizzazione informatica dei giovani che vivono nelle baraccopoli di Nairobi. Questa iniziativa fa parte di un progetto piu' ampio: [Linux Aids People](http://www.oses.it/cosa-facciamo/progetto-linux-aids-people-1)! Leggi il resto dell'articolo su [Oses.it](http://www.oses.it/cosa-facciamo/progetto-linux-aids-people-1/progetto-pc-in-the-slum/linux-in-the-slum/). Altre info su [cissiboy linux-e-le-speranze-africane](http://cissiboy.wordpress.com/2007/10/16/linux-e-le-speranze-africane/)