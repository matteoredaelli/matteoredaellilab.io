---
title: 'How to setup a multi node Hadoop cluster'
date: Thu, 07 Mar 2013 20:46:58 +0000
draft: false
tags: ['Apache', 'Hadoop', 'Linux', 'Me', 'OpenSource']
---

![](http://www.michael-noll.com/blog/uploads/Hadoop-multi-node-cluster-overview.png)

Read the interesting articles by Michael G. Noll

[Running Hadoop on Ubuntu Linux (Single-Node Cluster)](http://www.michael-noll.com/tutorials/running-hadoop-on-ubuntu-linux-single-node-cluster/)

[Running Hadoop on Ubuntu Linux (Multi-Node Cluster)](http://www.michael-noll.com/tutorials/running-hadoop-on-ubuntu-linux-multi-node-cluster/)