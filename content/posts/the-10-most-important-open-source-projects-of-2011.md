---
title: 'The 10 Most Important Open Source Projects of 2011'
date: Mon, 19 Dec 2011 20:13:22 +0000
draft: false
tags: ['OpenSource']
---

![](https://www.linux.com/images/stories/hadoop.jpg "hadoop")

[Hadoop](http://hadoop.apache.org/)

[Git](http://git-scm.com/)

[Cassandra](http://cassandra.apache.org/)

[LibreOffice](http://www.libreoffice.org/)

[OpenStack](http://openstack.org/)

[Jquery](http://jquery.com/)

[Nginx](http://nginx.org/)

[Node.js](http://nodejs.org/)

[Puppet](http://puppetlabs.com/)

See original [post](https://www.linux.com/news/featured-blogs/196-zonker/524082-the-10-most-important-open-source-projects-of-2011Did) at linux.com
