---
title: 'Natale 2010: riflessioni di un 40enne depresso'
date: Fri, 24 Dec 2010 18:28:30 +0000
draft: false
tags: ['Africa', 'Me', 'Volontariato']
---

Possibile che nel 2010 ci siano ancora persone che muoiono di freddo a Milano nella quasi totale indifferenza di tutti noi, delle istituzioni e della Chiesa? [![](http://milano.corriere.it/hermes_foto/2010/12/18/0LDMJU1C--180x140.jpg)](http://milano.corriere.it/milano/notizie/cronaca/10_dicembre_14/senzatetto-morta-giardini-18169973434.shtml?fr=correlati) [![](http://milano.corriere.it/media/foto/2010/12/14/799907_T--180x140.JPG "a")](http://milano.corriere.it/milano/notizie/cronaca/10_dicembre_14/senzatetto-morta-giardini-18169973434.shtml?fr=correlati) Ma forse e' meglio non pensarci e abbuffarsi nei cenoni e pranzi del Santo Natale.

\[youtube\]http://www.youtube.com/watch?v=Gup4hQnFWyA\[/youtube\]