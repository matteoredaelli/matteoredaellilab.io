---
title: 'Building a Cloud-Agnostic Serverless infrastructure with Knative'
date: Fri, 23 Nov 2018 08:16:05 +0000
draft: false
tags:
- serverless
- kubernetes
- knative
---

KNATIVE is Kubernetes-based platform to build, deploy, and manage modern serverless workloads

![](http://www.redaelli.org/matteo-blog/wp-content/uploads/2018/11/knative-1024x677.png)

"Knative provides a set of middleware components that are essential to build modern, source-centric, and container-based applications that can run anywhere: on premises, in the cloud, or even in a third-party data center. Knative components are built on Kubernetes and codify the best practices shared by successful real-world Kubernetes-based frameworks. It enables developers to focus just on writing interesting code, without worrying about the “boring but difficult” parts of building, deploying, and managing an application." \[[https://cloud.google.com/knative/](https://cloud.google.com/knative/)\]

"Knative has been developed by Google in close partnership with [Pivotal](https://content.pivotal.io/blog/knative-powerful-building-blocks-for-a-portable-function-platform), [IBM](https://www.ibm.com/blogs/cloud-computing/2018/07/24/ibm-cloud-google-knative-serverless/), [Red Hat,](https://blog.openshift.com/state-of-serverless-in-kubernetes-knative-and-openshift-cloud-functions/) and [SAP](https://blogs.sap.com/?p=696354)." \[[infoq.com](https://www.infoq.com/news/2018/07/knative-kubernetes-serverless)\]
