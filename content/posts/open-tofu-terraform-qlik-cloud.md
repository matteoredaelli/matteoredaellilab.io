---
title: 'DevOps Qlik Cloud - managing qlik cloud resources with OpenTofu / Terraform'
date: Tue, 10 Oct 2023 09:14:35 +0000
draft: false
tags: ['QLik', 'Tofu', 'Terraform', 'devops']
---

## Abstract

Open tofu (a Terraform fork) is an open-source infrastructure as code (IaC) tool that allows you to define, provision, and manage infrastructure as code. Terraform can be used to manage a wide range of infrastructure resources, including [Qlik Cloud](https://www.qlik.com) resources.

Here are some of the benefits of using OpenTofu/Terraform to manage Qlik Cloud resources:

   Consistency: Terraform allows you to define your infrastructure in a consistent and repeatable way. This can help to reduce errors and improve the overall quality of your infrastructure.
    
    Automation: Terraform can be used to automate the deployment and management of your Qlik Cloud resources. This can save you time and effort, and it can also help to improve the reliability of your infrastructure.
    
    Version control: Terraform allows you to version control your infrastructure configuration. This means that you can track changes to your infrastructure over time and easily roll back to previous versions if necessary.

To get started with OpenTofu/Terraform, you will need to install the OpenTofu/Terraform CLI tool. Once you have installed the OpenTofu/Terraform CLI, you can create a new Terraform configuration file. Terraform configuration files are written in a language called HashiCorp Configuration Language (HCL).

To manage Qlik Cloud resources with Terraform, you will need to use the Mastercard/terraform-provider-restapi provider. This provider allows you to use Terraform to manage any REST API, which makes it very flexible and powerful.


## Usage

Below I'll show how to create some Qlik groups, spaces and space assigments.

Create a folder and inside the following files:

File variables.tf

```terraform
variable "QLIK_APIKEY" {
  type = string
}

variable "QLIK_URI" {
  type = string
}
```

File versions.tf

```terraform
terraform {
  required_providers {
    restapi = {
      source = "mastercard/restapi"
      version = "1.18.2"
    }
  }
}
```

File providers.tf

```terraform
provider "restapi" {
  alias                = "qlik_saas"
  uri                  = "${var.QLIK_URI}"
  debug                = true
  write_returns_object = true

  headers = {
      Authorization = "Bearer ${var.QLIK_APIKEY}"
      "Content-Type" = "application/json"
  }
}
```

File qlik_groups.tf

```terraform
resource "restapi_object" "group_Developer" {
    provider = restapi.qlik_saas
    path = "/api/v1/groups"
    data = jsonencode({
        name: "Developer"
        status: "active"
        assignedRoles: []})
}

resource "restapi_object" "group_Italy" {
    provider = restapi.qlik_saas
    path = "/api/v1/groups"
    data = jsonencode({
        name: "Italy"
        status: "active"
        assignedRoles: []})
}

```

File qlik_space_italy.tf

```terraform
resource "restapi_object" "space_Italy" {
    provider = restapi.qlik_saas
    path = "/api/v1/spaces"
    data = jsonencode({
	name: "Italy"
	type: "managed"
	description: "Italy space" })
}

resource "restapi_object" "space_Italy_assigments_group_Developer" {
    provider = restapi.qlik_saas
    path = "/api/v1/spaces/${resource.restapi_object.space_Italy.id}/assignments"
    data =  jsonencode({
	type: "group"
	roles: ["consumer","contributor","dataconsumer","publisher"]
	assigneeId: "${resource.restapi_object.group_Developer.id}"
    })
}

resource "restapi_object" "space_Italy_assigments_group_Italy" {
    provider = restapi.qlik_saas
    path = "/api/v1/spaces/${resource.restapi_object.space_Italy.id}/assignments"
    data =  jsonencode({
	type: "group"
	roles: ["consumer","dataconsumer"]
	assigneeId: "${resource.restapi_object.group_Italy.id}"
    })
}
```

Now you can create your resources with

```bash
export TF_VAR_QLIK_APIKEY=xxxxxxxxxxxxxxxxxxxxxxxx
export TF_VAR_QLIK_URI=https://mytenant.eu.qlikcloud.com

tofu init
tofu plan -out myplan
tofu apply myplan
```


## Importing existing resources

You could also import in the terraform state any resources already/manually created with something like

```bash
tofu import restapi_object.space_Germany /api/v1/spaces/12222222
```

## Managing several environments/states

```bash
tofu workspace create dev
tofu workspace create prd
tofu workspace select dev
```

## References:
- [qlik-cloud-devops-terraform-opentofu github repository](https://github.com/matteoredaelli/qlik-cloud-devops-terraform-opentofu)
- [OpenTofu](https://opentofu.org/) witch is a fork of Terraform under the Linux Foundation unbrella. 
- [Mastercard/terraform-provider-restapi](https://github.com/Mastercard/terraform-provider-restapi)
- [Qlik cloud](https://www.qlik.com)
