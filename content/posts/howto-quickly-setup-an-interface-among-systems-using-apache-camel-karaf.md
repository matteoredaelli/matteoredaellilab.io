---
title: 'Howto quickly setup an interface among systems using Apache Camel / Karaf (OSGI)'
date: Wed, 25 Jul 2012 18:04:56 +0000
draft: false
tags: ['Apache', 'Camel', 'Database', 'ESB', 'jdbc', 'Karaf', 'Linux', 'mysql', 'OpenSource', 'osgi', 'sql server']
---

![](http://upload.wikimedia.org/wikipedia/commons/9/9c/Apache-camel-logo.png "Apache Camel")In the article [building system integrations with Apache Camel](https://docs.google.com/document/d/1oX4RnLWad4b2phlIRluKIZAw9jEakl3Gy61oE1A-AAw/edit) I’ll show how to create in 10 minutes an **integration between two databases** (**without writing any lines of java or c# code**):

*   looking for uses in the database MOODLE (mysql) with missing attributes
*   for each of that users retreiving the missing attributes from the database UPMS (m$ sql server) and then
*   adding the missing attributes to the database MOODLE

I'll use

*   [Apache Karaf](http://karaf.apache.org/) 2.2.8
*   [Apache Camel](http://camel.apache.org/) 2.10.0

under Linux.   Any suggestions and comments are welcome! Matteo
