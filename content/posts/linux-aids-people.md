---
title: 'Linux Aids People'
date: Sun, 11 Nov 2007 07:29:50 +0000
draft: false
tags: ['Africa', 'Me', 'No profit', 'OpenSource']
---

Nell'ambito delle attività solidali di lotta al sottosviluppo, alla povertà, al disagio, [OSES](http://www.oses.it) ha avviato il progetto permanente _[Linux Aids People](http://www.oses.it/cosa-facciamo/programma-linux-aids-people/programma-linux-aids-people)_ che mira alla raccolta e riconversione, con soluzioni Open Source, di Personal Computer da destinare ad uso didattico nelle scuole dei paesi in via di sviluppo.
---
### Comments:
#### 
[Federico]( "federico.stufa@fastwebnet.it") - <time datetime="2007-11-22 14:18:04">Nov 4, 2007</time>

Ciao Matteo, mi sono iscritto sul sito di OsES. Potrei sapere come collaborare? Grazie Federico
<hr />
