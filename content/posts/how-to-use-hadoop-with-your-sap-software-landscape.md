---
title: 'How to Use Hadoop with your SAP Software Landscape'
date: Wed, 31 Jul 2013 14:04:08 +0000
draft: false
tags: ['Me']
---

![](http://de.sap.info/wp-content/uploads/2012/11/powered-by-SAP-HANA.jpg)

CIO Guide on Big Data “[How to Use Hadoop with your SAP Software Landscape](http://www.saphana.com/docs/DOC-3777)” just released

![](http://hadoop.apache.org/images/hadoop-logo.jpg)