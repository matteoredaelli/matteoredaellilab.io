---
title: 'Apache Camel: how to collect data from twitter'
date: Sat, 29 Dec 2012 22:13:34 +0000
draft: false
tags: ['Apache', 'Camel', 'eai', 'Java', 'osgi', 'SOA', 'twitter']
---

![](http://basementcoders.com/wp-content/uploads/2010/08/camel-logo.png)

In the article [Apache Camel: how to collect data from twitter](https://docs.google.com/document/d/1m1NrjtnPF5QPd2l4YzPPkrUGja8oZQzrlUGn1_7jlGQ/edit),

I'll show howto save all tweets about Pirelli to files in .json format ..
