---
title: 'Using curl for querying Active Directory / LDAP from command line'
date: Fri, 18 May 2012 10:01:57 +0000
draft: false
tags:
- curl
- ldap
- active directory
- commandline
---

For quering Active Directory /LDAP from command line, you can run something like

```
curl -u DOMAIN\\\\MYACCOUNT:MYPASSWORD   \\
 "ldap://dc.redaelli.org:3268/OU=users,DC=redaelli,DC=org?memberOf,sAMAccountName?sub?(sAMAccountName=matteo)"
```
---
### Comments:
####
[Saqib Ali](https://hivemined.net "docbook.xml@gmail.com") - <time datetime="2018-12-03 23:35:10">Dec 1, 2018</time>

Super thanks for posting this!
<hr />
