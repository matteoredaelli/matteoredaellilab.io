---
title: 'London Olympics prediction for the 100m final with R and Strategico'
date: Mon, 23 Jul 2012 08:03:05 +0000
draft: false
tags: ['Me']
---

After reading the interesting article [London Olympics and a prediction for the 100m final](http://www.r-bloggers.com/london-olympics-and-a-prediction-for-the-100m-final/)  here you can find an alternative statistical prediction using [R](http://www.r-project.org/) and [Strategico](http://code.google.com/p/strategico/). Look at this [excel](https://docs.google.com/spreadsheet/ccc?key=0Ai15RDmCj2SsdF9uLWc2MHhUV0pkY0tNS09GX2F6anc) for details ![](https://docs.google.com/spreadsheet/oimg?key=0Ai15RDmCj2SsdF9uLWc2MHhUV0pkY0tNS09GX2F6anc&oid=1&zx=f5hlwh1v5d3b "olympics prediction 100m final")