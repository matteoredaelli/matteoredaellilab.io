---
title: 'After Oracle also SAP has discovered and integrated R'
date: Sun, 18 Mar 2012 20:06:15 +0000
draft: false
tags: ['R', 'SAP',  'Oracle',  'Opensource']
---

After Oracle with [Oracle R Enterprise](http://www.oracle.com/technetwork/database/options/advanced-analytics/r-enterprise/index.html), now also [SAP has adopted R](http://www.slideshare.net/JitenderAswani/na-6693-r-and-sap-hana-dkom-jitenderaswanijensdoeprmund): SAP HANA provides in-database analytics based on R with its SAP BusinessObjects Predictive Analysis module. For more info about R, please get a look at this [R introduction](https://docs.google.com/present/view?id=dfr68gz6_242f9zn26dw&revision=_latest&start=0&theme=blank&cwj=true)...
