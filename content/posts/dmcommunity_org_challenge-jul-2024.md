---
title: Smart investment Problem with Prolog
date: 2024-07-14
draft: false
tags:
- prolog
- programming
---

Below my #prolog solution for [Smart investment problem](https://dmcommunity.org/challenge/challenge-july-2024/).

It is a sample of [Linear Programming](https://en.wikipedia.org/wiki/Linear_programming) using Prolog.

A client of an investment firm has $10000 available for investment. He has instructed that his money be invested in particular stocks, so that no more than $5000 is invested in any one stock but at least $1000 be invested in each stock. He has further instructed the firm to use its current data and invest in the manner that maximizes his overall gain during a one-year period.

``` prolog
?- sol(S), variable_value(S, x1, QuantityABC), variable_value(S, x2, QuantityXYZ), variable_value(S, x3, QuantityTTT), variable_value(S, x4, QuantityLMN).
```

``` text
QuantityABC = 120,
QuantityXYZ = 20,
QuantityTTT = 10,
QuantityLMN = 200.
```

Below the Prolog code

``` prolog
:- use_module(library(simplex)).

sol(S) :-
    gen_state(S0),
    my_constraints(S0, S1),
    maximize([35*x1, 60*x2, 125*x3, 40*x4], S1, S).

my_constraints -->
    constraint([25*x1] >= 1000),
    constraint([25*x1] =< 5000),
    constraint([50*x2] >= 1000),
    constraint([50*x2] =< 5000),
    constraint([100*x3] >= 1000),
    constraint([100*x3] =< 5000),
    constraint([25*x4] >= 1000),
    constraint([25*x4] =< 5000),
    constraint([25*x1, 50*x2, 100*x3, 25*x4] =< 10000),
    constraint([x1] >= 0),
    constraint([x2] >= 0),
    constraint([x3] >= 0),
    constraint([x4] >= 0).
```
