---
title: 'Using chrome/firefox programmatically for navigating a (Qliksense) website and taking screenshots'
date: 2020-09-08
draft: false
tags:
- clojure
- automation
- web scrapying
- chrome
- firefox
- phantomjs
- safari
- qliksense
---
How do you know if the user interface (UI) of a website you're developing works as it should and the site as a whole delivers the optimal user experience (UX)? Headless browsers give you a fast, lightweight way to automate high-level actions and get an idea of how well your site operates in common scenarios.

Links to Headless browers:
- [Chrome](https://sites.google.com/a/chromium.org/chromedriver/)
- [Firefox](https://github.com/mozilla/geckodriver/releases)
- [Phantomjs](http://phantomjs.org/download.html)
- [Safari](https://webkit.org/blog/6900/webdriver-support-in-safari-10/)

I found the nice clojure library [etaoin](https://github.com/igrishaev/etaoin) for using a browser programmatically. Below my code

```clojure
(ns org.redaelli.qlik-bot-navigation
  (:use [etaoin.api])
  (:require [etaoin.keys :as k])
  (:gen-class))

(defn -main
  "Navigate qliksense pages and then take a screeshot."
  [& args]

  (with-chrome {} ff ;; additional options first, then bind name
	(doto ff
	  (go "https://qliksense.redaelli.org/")
	  ;; form login
	  (fill-multi {:username-input "REDAELLI\\matteo"
				   :password-input "MySillyPassword"})
	  (click {:tag :input
			  :type :submit})

	  (wait-visible {:class "lui-list__item lui-list__action q-li-stream"})

	  ;; enter the first stream
	  (click {:tag :li
			  :class "lui-list__item lui-list__action q-li-stream"})

	  ;; take a screenshot
	  (screenshot (java.io.File. "screenshot.png")))))
```
