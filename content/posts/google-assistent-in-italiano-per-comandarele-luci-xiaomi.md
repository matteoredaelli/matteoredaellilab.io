---
title: 'Google Assistent in italiano per comandarele luci Xiaomi'
date: Sat, 17 Mar 2018 21:33:46 +0000
draft: false
tags: ['Me']
---

![](http://www.redaelli.org/matteo-blog/wp-content/uploads/2018/03/google-assistant-169x300.png)Finalmente posso comandare le luci (xiaomi) di casa mia in italiano con Google Assistant! Per esempio funzionano le frasi

*   accendi le luci
*   spegni le luci
*   luce rossa
*   luce banca calda
*   ...