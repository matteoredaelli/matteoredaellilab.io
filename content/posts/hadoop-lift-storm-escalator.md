---
title: '#Hadoop : lift = #Storm : escalator'
date: Sun, 16 Feb 2014 20:14:14 +0000
draft: false
tags: ['Me']
---

[Apache Hadoop](http://hadoop.apache.org/) is not good for realtime computing: please evaluate [Apache Storm](http://storm.incubator.apache.org/) (from twitter) or [Apache Spark](http://spark.incubator.apache.org/)

[![storm-vs-hadoop](http://www.redaelli.org/matteo-blog/wp-content/uploads/2014/02/storm-vs-hadoop.png)](http://www.slideshare.net/edvorkin/storm-intro-v8)

References:

[http://www.slideshare.net/edvorkin/storm-intro-v8](http://www.slideshare.net/edvorkin/storm-intro-v8)

[http://www.infoq.com/news/2014/01/Spark-Storm-Real-Time-Analytics](http://www.infoq.com/news/2014/01/Spark-Storm-Real-Time-Analytics)