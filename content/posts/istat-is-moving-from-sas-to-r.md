---
title: 'ISTAT is moving from SAS to R'
date: Fri, 05 Jun 2009 14:49:43 +0000
draft: false
tags: ['Me', 'OpenSource']
---

"[Istat](http://www.istat.it/english/) \[..\] helps to develop '[R Project](http://www.r-project.org/)', a statistical open source application. "We have donated software libraries to R, and are moving away from using SAS, the proprietary alternative to R. We contributed to the statistical application Adamsoft, which is being developed at the Caspur computing-lab, just two kilometres away from our institute." Vaccari says **adopting open source** requires institutes to make adjustments in their organisational culture. "It **changes how people work, from 'I'm important because I'm a bottleneck, to I'm important because I share things**'." [Read it](http://www.osor.eu/news/it-statistics-institute-moving-to-open-source-increases-cooperation)!