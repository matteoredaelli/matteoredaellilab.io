---
title: 'Detrazione per gli abbonamenti ai mezzi di trasporto pubblici'
date: Wed, 19 Mar 2008 08:38:42 +0000
draft: false
tags: ['Trasporti', 'Uncategorized']
---

Finalmente la detrazione per gli abbonamenti ai mezzi di trasporto pubblici: art. 1, comma 309, legge n. 244 del 27 dicembre 2007. Ecco il testo del [rimborso\_abbonamenti.pdf](http://matteoredaelli.files.wordpress.com/2008/03/rimborso_abbonamenti.pdf "rimborso_abbonamenti.pdf")!