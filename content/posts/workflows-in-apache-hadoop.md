---
title: 'Workflows in Apache Hadoop'
date: Fri, 03 Oct 2014 20:20:43 +0000
draft: false
tags:
- apache
- workflow
- hadoop
- bigdata
- oziee
- luigi
- airflow
---

[![](http://oozie.apache.org/images/oozie_200x.png)](http://oozie.apache.org/)![](https://raw.githubusercontent.com/spotify/luigi/master/doc/luigi.png) How to orchestrate your Hadoop Jobs? Possible solutions are:

*   [Apache Oziee](http://oozie.apache.org/) included in the top Hadoop distributions
*   [Azkaban](http://azkaban.github.io/) from [LinkedIn](http://data.linkedin.com/opensource/azkaban)
*   [Luigi](https://github.com/spotify/luigi) from Spotify
*   [Apache Airflow](https://airflow.apache.org/) from AirBnb

See for instance a comparison among luigi, airflow and pinball at [http://bytepawn.com/luigi-airflow-pinball.html](http://bytepawn.com/luigi-airflow-pinball.html)
