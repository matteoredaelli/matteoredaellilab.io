---
title: 'E'' la fine della storia'
date: Mon, 25 Feb 2008 19:18:22 +0000
draft: false
tags: ['Poesie', 'Uncategorized']
---

_"E' la fine della storia, e non lo sai._ _Lui e' li', in piedi davanti alla finestra, e tu non gli perdoni di schermare la luce._ _Non e' lui che vedi, ma il giorno, cui lui impedisce di entrare. Inizia cosi"_ \[Brigitte Giraud\], tratto dal quotidiano City, 25-02-2008