---
title: 'Erlang at Facebook, SAP, ...'
date: Fri, 26 Jun 2009 09:45:03 +0000
draft: false
tags: ['Erlang', 'Open', 'OpenSource']
---

Erlang At? Please add a comment if you use it in your company! Erlang at BBC

*   [http://www.erlang-factory.com/upload/presentations/147/EndaFarrell-ErlangFactoryLondon2009-ErlangattheBBC.pdf](http://www.erlang-factory.com/upload/presentations/147/EndaFarrell-ErlangFactoryLondon2009-ErlangattheBBC.pdf)

Erlang at Facebook

*   [Functional Programming at Facebook by Chris Piro and Eugene Letuchy](http://cufp.galois.com/2009/slides/PiroLetuchy.pdf).
*   [http://www.erlang-factory.com/upload/presentations/31/EugeneLetuchy-ErlangatFacebook.pdf](http://www.erlang-factory.com/upload/presentations/31/EugeneLetuchy-ErlangatFacebook.pdf)
*   [http://www.process-one.net/en/blogs/article/facebook\_chat\_supports\_xmpp\_with\_ejabberd/](http://www.process-one.net/en/blogs/article/facebook_chat_supports_xmpp_with_ejabberd/)

Erlang at Github

*   [http://github.com/blog/112-supercharged-git-daemon](http://github.com/blog/112-supercharged-git-daemon)

Erlang at SAP

*   [http://www.erlang-factory.com/upload/presentations/57/SumeetBajaj\_ErlangatSAP.pdf](http://www.erlang-factory.com/upload/presentations/57/SumeetBajaj_ErlangatSAP.pdf)

Erlang at 37Signals

*   [http://beebole.com/en/blog/erlang/37signals-dives-into-erlang/](http://beebole.com/en/blog/erlang/37signals-dives-into-erlang/)

Other presentations at

*   [http://www.erlang-factory.com/conference/SFBayAreaErlangFactory2009/talks](http://www.erlang-factory.com/conference/SFBayAreaErlangFactory2009/talks)
*   [http://www.erlang-factory.com/conference/London2009/talks](http://www.erlang-factory.com/conference/London2009/talks)
*   [http://2009.scandevconf.se/db/Erlang\_good\_new\_bad\_news\_how\_to\_win.pdf](http://2009.scandevconf.se/db/Erlang_good_new_bad_news_how_to_win.pdf)
---
### Comments:
####
[The Interactive Internet is quite near&#8230; the new facebook chat with XMPP&#8230; &laquo; Matteo Redaelli](http://www.redaelli.org/matteo-blog/archives/507 "") - <time datetime="2010-02-13 16:20:57">Feb 6, 2010</time>

\[...\] new facebook chat uses an OpenSource software witten in Erlang, ejabberd!! That will be also used by next moodle 2.0 chat \[...\]
<hr />
####
[Matteo](http://www.redaelli.org "matteo.redaelli@gmail.com") - <time datetime="2009-10-31 11:22:02">Oct 6, 2009</time>

http://stackoverflow.com/questions/1636455/where-is-erlang-used-and-why
<hr />
