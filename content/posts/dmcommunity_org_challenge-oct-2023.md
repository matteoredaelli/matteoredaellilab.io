---
title: Coins challenge Oct 2023 with Prolog
date: 2023-11-12
draft: false
tags:
- prolog
- programming
---

My #prolog solution for "Coins" proposed by [dmcommunity.org challenge Oct 2023](https://dmcommunity.org/challenge/challenge-oct-2023/).

Suppose you need to pay 1 Euro. In how many different ways you can do it via 1c, 2c, 5c, 10c, 20c, 50c and 1 Euro coins?


``` prolog
%%  findall(S, sol(100, S), All), maplist(writeln, All).

%% ...
%% [94,3,0,0,0,0,0]
%% [95,0,1,0,0,0,0]
%% [96,2,0,0,0,0,0]
%% [98,1,0,0,0,0,0]
%% [100,0,0,0,0,0,0]
%% ...
```

Below the code

``` prolog

:- use_module(library(clpfd)).

sol(TOT, [C1, C2, C5, C10, C20, C50, C100]) :-
    C1   in 0..100,
    C2   in 0..50,
    C5   in 0..20,
    C10  in 0..10,
    C20  in 0..5,
    C50  in 0..2,
    C100 in 0..1,
    TOT #= C1*1 + C2*2 + C5*5 + C10*10 + C20*20 + C50*50 + C100*100,
    label([C1, C2, C5, C10, C20, C50, C100]).
```
