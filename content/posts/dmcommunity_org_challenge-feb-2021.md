---
title: Medical Services challenge with Prolog
date: 2021-02-02
draft: false
tags:
- prolog
- programming
---

My #swi #prolog solution for "Benchmark 'Medical Services'" proposed by [dmcommunity.org challenge Feb 2021](https://dmcommunity.org/challenge/challenge-feb-2021/) can be tested running a public docker image

``` bash
	docker run -p 8888:8888 -d --name matteoredaelli/dmcommunity_org_2021_02:latest
	wget https://github.com/DMCommunity/dmcommunity_shared/raw/master/MedicalServices.json
	curl -XPOST -d @MedicalServices.json -H "Accept: application/json" -H "Content-type: application/json" http://localhost:8888/many
```

The decision table is a set of prolog facts like

``` prolog
decision_table('Office','acupuncture','PL123','L','Y','N','2015-01-01','2023-12-31','N','N','N').
decision_table('Outpatient','acupuncture','PL123','L','Y','N','2015-01-01','2023-12-31','N','N','N').
decision_table('Inpatient','acupuncture','PL123','L','N','N','2015-01-01','2023-12-31','N','N','N').
```

The core of the solution is inside the rules.pl file

``` prolog
parse_input(json([placeOfService=PlaceOfService,
		  type=Type,
		  plan=Plan,
		  groupSize=GroupSize,
		  inNetwork=InNetwork,
		  isCovered=IsCovered,
		  dateOfService=DateOfService,
		  coveredInFull=_,
		  copay=_,
		  coInsurance=_]),
		json([placeOfService=PlaceOfService,
		  type=Type,
		  plan=Plan,
		  groupSize=GroupSize,
		  inNetwork=InNetwork,
		  isCovered=IsCovered,
		  dateOfService=DateOfService,
		  coveredInFull=CoveredInFull,
		  copay=Copay,
		  coInsurance=CoInsurance])):-
	decision_table(PlaceOfService,
			   Type,
			   Plan,
			   GroupSize,
			   InNetwork,
			   IsCovered,
			   DateOfService1,
			   DateOfService2,
			   CoveredInFull,
			   Copay,
			   CoInsurance),
	%% check date: must be between the two dates in decision_table
	atom_string(DateOfService, DateOfServiceString),
	atom_string(DateOfService1, DateOfService1String),
	atom_string(DateOfService2, DateOfService2String),
	DateOfService1String @=< DateOfServiceString,
	DateOfServiceString  @=< DateOfService2String.
```

The file server.pl contains the code for the rest server that exposes the core functions as http POST requests.

## References:

- https://gitlab.com/matteoredaelli/dmcommunity_2021_02
- https://hub.docker.com/repository/docker/matteoredaelli/dmcommunity_org_2021_02
