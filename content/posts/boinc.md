---
title: 'Boinc'
date: Tue, 01 Apr 2008 06:33:56 +0000
draft: false
tags: ['Grid Computing', 'No profit', 'Uncategorized']
---

![](http://boinc.berkeley.edu/logo/www_logo.gif)Use the idle time on your computer (Windows, Mac, or Linux) to cure diseases, study global warming, discover pulsars, and do many other types of scientific research. It's safe, secure, and easy with **[boinc](http://boinc.berkeley.edu/)**
---
### Comments:
#### 
[tramp](http://mimo.splinder.com "vittorio.re@gmail.com") - <time datetime="2008-04-16 10:22:12">Apr 3, 2008</time>

The idea is surely praiseworthy and I was about to subscribe. But suddenly I stopped and some strange thoughts started to surf into my mind. Why should I give my free help to a foreign university / department? I would be happier if I could give this same contribution to an italian organization. Maybe I'm wrong and I'm a bit selfish, but I think: "what would those people do with the results of their studies?"; "will they make all the other peolple pay for that?". So I say "NO, I won't subscribe!"
<hr />
