---
title: 'A new blog for me!'
date: Tue, 05 Jan 2010 18:31:11 +0000
draft: false
tags: ['facebook', 'Me', 'redaelli', 'twitter']
---

My blog now lives at [redaelli.org](http://www.redaelli.org/matteo/): new posts will be published automatically also in my facebook and twitter profiles. But I'm looking also at blogger: I'm playing with [http://redaellicarate.blogspot.com/](http://redaellicarate.blogspot.com/)