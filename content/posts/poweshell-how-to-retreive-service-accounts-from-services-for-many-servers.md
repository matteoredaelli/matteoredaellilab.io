---
title: 'Powershell: how to retreive service accounts from services for many servers'
date: Mon, 11 May 2015 15:28:02 +0000
draft: false
tags: ['powershell']
---

Below a sample script```
$hosts = Get-Content "hosts.txt"
foreach ($myhost in $hosts) {
  Get-WmiObject win32\_service -ComputerName $myhost|where {$\_.StartMode -eq "Auto"} | where {$\_.StartName -like "\*.\*"} | foreach {write-host $myhost, $\_.Startname}
}
```
