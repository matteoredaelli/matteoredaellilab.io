---
title: 'Google Predictor API - release 1.2'
date: Thu, 19 May 2011 19:03:32 +0000
draft: false
tags: ['Google', 'Me', 'WebService']
---

The [Google Predictor API](http://googlecode.blogspot.com/2011/05/google-prediction-api-helps-all-apps-to.html) V1.2 is out! "The Prediction API provides pattern-matching and machine learning capabilities. Given a set of data examples to train against, you can create applications that can perform the following tasks:

*   Given a user's past viewing habits, predict what other movies or products a user might like.
*   Categorize emails as spam or non-spam.
*   Analyze posted comments about your product to determine whether they have a positive or negative tone.
*   Guess how much a user might spend on a given day, given his spending history.

" \[see [more](http://code.google.com/apis/predict/docs/getting-started.html#whatisprediction)\] There are [libraries and sample code](http://code.google.com/apis/predict/docs/libraries.html) for Bash, Java, .Net, Python, Ruby, ... For details of samples look at [guide](http://code.google.com/apis/predict/docs/developer-guide.html) Links:

*   [Ford & Google predictor](http://forddealervirginia.com/ford-google-predictor-future-api.html) (also [here](http://www.businessgreen.com/bg/news/2072141/ford-google-tie-predict-car-trips-optimise-plug-hybrids))
*   [Ford & Google Trends](http://www.google.com/googleblogs/pdfs/google_predicting_the_present.pdf)

As alternative, for [Long Term Prediction over Time Series](http://www.redaelli.org/matteo-blog/projects/r-strategico/), you can have a look at [Strategico](http://www.redaelli.org/matteo-blog/projects/r-strategico/).