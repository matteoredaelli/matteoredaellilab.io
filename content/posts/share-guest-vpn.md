---
title: 'Sharing Windows guest vpn connection'
date: 2021-10-03
draft: false
tags: ['Linux', 'vpn', 'networking']
---

## Abstract

I have a linux workstation with a corporate guest Windows image running on kvm. I would like to share the guest windows vpn connection with my linux host workstation.

My linux host has one network: virbr0 (public) with an ip 192.168.122.x

My vpn/corporate subnet is 10.150.0.0/16

## Solution

Create on linux/kvm a new host only/private net ( I have an ip 192.168.100.x) and enable it in the windows guest os (ip 192.168.100.236)

![virbr0](/img/kvm-virbr0.png)
![virbr1](/img/kvm-virbr1.png)

On the guest OS you should have several networks. I named them: virbr0 (public/internet), virbr1 (private, host only), vpn (created/used by your vpn client).

## Solution with sshuttle

([sshuttle](https://sshuttle.readthedocs.io/en/stable/index.html)): connecting to many remote hosts like native connections (without socks and pac files).

"Sshuttle is not exactly a VPN, and not exactly port forwarding. It’s kind of both, and kind of neither."

"Sshuttle assembles the TCP stream locally, multiplexes it statefully over an ssh session, and disassembles it back into packets at the other end. So it never ends up doing TCP-over-TCP. It’s just data-over-TCP, which is safe."

I installed sshuttle with "apt-get install sshuttle" on my debian laptop

Then I installed sshd and python on the guest windows (I used cygwin).

Finally I was able to run:

```
cp /etc/resolv.conf.corporate /etc/resolv.conf
sshuttle -r matteo@192.168.100.239 10.150.0.0/16
```

## Solution with network sharing

### Guest Windows side

Start your vpn in your windows guest OS (I saw that checkpoint - I suppose for securty reason - changes the ip address of the host net and share options of its own net)

Share the vpn net with the host only net: below my screenshots

![share](/img/guest-windows-net-share.png)

![final](/img/guest-windows-net-final.png)

### Host Linux side

Enable the required routing

```bash
cp /etc/resolv.conf.corporate /etc/resolv.conf
ip route add 10.150.0.0/15 via 192.168.100.236 dev virbr1
```

## PS

One year ago I wrote an article with some useful networking tricks https://matteoredaelli.netlify.app/news/my-networking-survival-kit/
