---
title: 'Mailman: Howto recover a mailinglist admin password'
date: Mon, 09 May 2016 11:28:54 +0000
draft: false
tags:
- mailman
---

[![](https://www.gnu.org/software/mailman/images/logo2010-2.jpg)](https://www.gnu.org/software/mailman/)Do you use [mailman](https://www.gnu.org/software/mailman/) and have you lost your admin password of you mailman list? You can easily reset it with```
_sudo /usr/lib/mailman/bin/change\_pw -l listname -p newpass_
```
