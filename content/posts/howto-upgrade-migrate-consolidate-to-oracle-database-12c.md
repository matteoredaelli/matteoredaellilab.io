---
title: 'Howto Upgrade, Migrate & Consolidate to Oracle Database 12c'
date: Tue, 12 May 2015 15:00:37 +0000
draft: false
tags: ['Me']
---

[![oracle-support](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/05/oracle-support.jpg)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2015/05/oracle-support.jpg) I suggest to visit the site [https://blogs.oracle.com/UPGRADE](https://blogs.oracle.com/UPGRADE/) and the attachment pdf [**Upgrade, Migrate & Consolidate to Oracle Database 12c**](http://apex.oracle.com/pls/apex/f?p=202202:2:::::P2_SUCHWORT:migrate12c "Upgrade, Migrate and Consolidate to 12c")