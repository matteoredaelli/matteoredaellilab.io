---
title: 'oRestE: a REST interface to your databases with Erlang'
date: Wed, 07 Oct 2009 07:24:52 +0000
draft: false
tags: ['Me']
---

[![](http://www.redaelli.org/matteo-blog/wp-content/uploads/2009/10/oreste1.png "oRestE")](http://www.redaelli.org/matteo-blog/wp-content/uploads/2009/10/oreste1.png) [oRestE](http://code.google.com/p/oreste/) gives you a fast way to expose your database with a restful interface oRestE is built on top of [ERLANG](http://www.erlang.org/) ([Introduction](http://www.slideshare.net/kenpratt/intro-to-erlang)), [MOCHIWEB](http://code.google.com/p/mochiweb/) and [WEBMACHINE](http://bitbucket.org/justin/webmachine) ([webmachine.pdf](http://www.erlang-factory.com/upload/presentations/60/sheehy_factory-webmachine.pdf)) oRestE it is light, stable and fast! thanks to ERLANG/OTP and MOCHIWEB