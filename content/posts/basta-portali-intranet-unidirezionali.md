---
title: 'Basta Portali Intranet Unidirezionali!'
date: Sat, 27 Dec 2008 13:19:47 +0000
draft: false
tags: ['Me']
---

Lavoro per un'Azienda che, come altre di vecchio stampo, persiste con un modello di Intranet unidirezionale (dall'alto verso il basso dove il dipendente e' passivo)  che non stimola e incentiva la collaborazione e la condivisione del know-how. Purtroppo e' una questione di mentalita' e non di strumenti. Strumenti validi, robusti, gratuiti, e OpenSource ce ne sono... Leggete [qui'.](http://matteoredaelli.wordpress.com/2008/12/27/basta-portali-intranet-unidirezionali/wordpress-mu/) [http://www.slideshare.net/matteoredaelli/wordpress-mu-presentation](http://www.slideshare.net/matteoredaelli/wordpress-mu-presentation) [http://www.slideshare.net/folletto/wordpress-as-cms](http://www.slideshare.net/folletto/wordpress-as-cms) [http://www.slideshare.net/msincome/how-to-use-wordpress-widgets](http://www.slideshare.net/msincome/how-to-use-wordpress-widgets) [http://www.slideshare.net/msincome/wordpress-css-editing](http://www.slideshare.net/msincome/wordpress-css-editing) [http://www.slideshare.net/bazza/high-performance-wordpress](http://www.slideshare.net/bazza/high-performance-wordpress)