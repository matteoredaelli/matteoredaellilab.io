---
title: 'Google uses R: a new open-source package for estimating causal effects in time series'
date: Tue, 16 Sep 2014 20:25:36 +0000
draft: false
tags:
- R
- google
---

[![](http://2.bp.blogspot.com/-EmqnkfLkz48/VBDAnBtrU6I/AAAAAAAAYiU/w23tbIXCCTE/s1600/image00.png)](http://google-opensource.blogspot.de/2014/09/causalimpact-new-open-source-package.html) In this blog post Google confirms its adoption of the opensource statistical environment [R](http://www.r-project.org/) (see my [R introduction](https://docs.google.com/presentation/d/16YdyCKAi-YTEg8r3ox1uIWmt7Sw78BKNSJOAWYvPdTg/present#slide=id.i0)) releasing a new R package.. "How can we measure the number of additional clicks or sales that an AdWords campaign generated? How can we estimate the impact of a new feature on app downloads? How do we compare the effectiveness of publicity across countries? In principle, all of these questions can be answered through **causal inference \[**...\] **How the package works** The _CausalImpact_ R package implements a Bayesian approach to estimating the causal effect of a designed intervention on a time series. Given a response time series (e.g., clicks) and a set of control time series (e.g., clicks in non-affected markets, clicks on other sites, or [Google Trends](http://www.google.com/trends/) data), the package constructs a Bayesian structural time-series model with a built-in spike-and-slab prior for automatic variable selection. This model is then used to predict the counterfactual, i.e., how the response metric would have evolved after the intervention if the intervention had not occurred." Read the f[ull Google blog post](http://google-opensource.blogspot.de/2014/09/causalimpact-new-open-source-package.html)
