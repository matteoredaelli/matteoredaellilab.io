---
title: 'DevOps Tableau Cloud - managing tableau cloud resources with OpenTofu / Terraform'
date: Wed, 26 Jun 2024 09:14:35 +0000
draft: false
tags: ['Tableau', 'Tofu', 'Terraform', 'devops']
---

## Abstract

[OpenTofu](https://opentofu.org/) (a Terraform fork, community-driven and managed by the Linux Foundation) is an open-source infrastructure as code (IaC) tool that allows you to define, provision, and manage infrastructure as code. Terraform can be used to manage a wide range of infrastructure resources, including Tableau Cloud resources.

Here are some of the benefits of using OpenTofu/Terraform to manage [Tableau Cloud](https://www.tableau.com/) resources:

   Consistency: Terraform allows you to define your infrastructure in a consistent and repeatable way. This can help to reduce errors and improve the overall quality of your infrastructure.
    
    Automation: Terraform can be used to automate the deployment and management of your Tableau Cloud resources. This can save you time and effort, and it can also help to improve the reliability of your infrastructure.
    
    Version control: Terraform allows you to version control your infrastructure configuration. This means that you can track changes to your infrastructure over time and easily roll back to previous versions if necessary.

To get started with OpenTofu/Terraform, you will need to install the OpenTofu/Terraform CLI tool. Once you have installed the OpenTofu/Terraform CLI, you can create a new Terraform configuration file. Terraform configuration files are written in a language called HashiCorp Configuration Language (HCL).



## Usage

Below I'll show how to create some Tableau groups, users and projects.

Create a folder and inside the following files:

File versions.tf

```terraform
terraform {
    required_providers {
	tableau = {
	    source = "GtheSheep/tableau"
	    version = "0.0.17"
	}
    }
}
```

File providers.tf

```terraform
provider "tableau" {
    server_url = "https://eu-west-1a.online.tableau.com"
    server_version = "3.13"
    site           = "redaelli"
}
```

File tableau_groups.tf

```terraform
resource "tableau_group" "group_developer" {
    provider = tableau
    name     = "developer"
    minimum_site_role = "Creator"
}

```

File tableau_users.tf

```terraform
resource "tableau_user" "matteo-redaelli" {
    provider = tableau
    auth_setting = "ServerDefault"
    name  = "matteo@redaelli.org"
    email = "matteo@redaelli.org"
    full_name = "matteo@redaelli.org"
    site_role = "Viewer"
}

resource "tableau_group_user" "group_developer_user_matteo-redaelli" {
    provider = tableau
    group_id = "${resource.tableau_group.group_developer.id}"
    user_id  = "${resource.tableau_user.user_matteo-redaelli.id}"
}
```

File tableau_projects.tf

```terraform
resource "tableau_project" "project_crm" {
    provider = tableau
    name     = ""
    description = "CRM projects"
    parent_project_id = ""
    content_permissions = "ManagedByOwner"
}

resource "tableau_project" "project_crm_sales" {
    provider = tableau
    name     = "CRM Sales"
    description = "CRM Sales"
    parent_project_id = "${resource.tableau_project.project_crm.id}"
    content_permissions = "ManagedByOwner"
}
```

Now you can create your resources with

```bash

tofu init
tofu plan -out myplan
tofu apply myplan
```


## References:

- [OpenTofu](https://opentofu.org/) is a fork of Terraform under the Linux Foundation unbrella. 
- [GtheSheep/terraform-provider-tableau](https://github.com/GtheSheep/terraform-provider-tableau)
- [Tableau](https://www.tableau.com/)
