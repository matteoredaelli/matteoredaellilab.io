---
title: 'Installing Nodejs oracledb module on Suse SLES 11'
date: Thu, 11 Feb 2016 09:52:30 +0000
draft: false
tags:
- nodejs
- oracle
- suse
---

[![](http://www.oracle.com/ocom/groups/public/@otn/documents/digitalasset/2550783.png)](https://github.com/oracle/node-oracledb/blob/master/INSTALL.md)For a quick tutorial about installing Oracle module for [Nodejs](https://nodejs.org/) ([oracledb](https://github.com/oracle/node-oracledb)) on Suse SLES, follow the info at [Node-OracleDB Installation](https://github.com/oracle/node-oracledb/blob/master/INSTALL.md) but remember to use the gcc compiler release 5.0```
export ORACLE\_HOME=/home/oracle/instantclient\_12\_1
export LD\_LIBRARY\_PATH=$LD\_LIBRARY\_PATH:$ORACLE\_HOME
export TNS\_ADMIN=$ORACLE\_HOME
export OCI\_LIBRARY\_PATH=$ORACLE\_HOME
export OCI\_LIB\_DIR=$ORACLE\_HOME
export OCI\_INC\_DIR=$ORACLE\_HOME/sdk/include
``````
CC=gcc-5 CXX=g++-5 npm install oracledb
```
