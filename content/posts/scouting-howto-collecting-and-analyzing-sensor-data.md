---
title: 'Scouting howto collecting and analyzing  sensor data with Hadoop or other NOSQL databases'
date: Wed, 10 Sep 2014 19:44:01 +0000
draft: false
tags: ['spark']
---

**[Collecting and analyzing sensor data with hadoop or other no sql databases](https://www.slideshare.net/matteoredaelli/collecting-analyzing-sensor-data-with-hadoop-or-other-no-sql-databases "Collecting and analyzing sensor data with hadoop or other no sql databases")** from **[Matteo Redaelli](http://www.slideshare.net/matteoredaelli)**
