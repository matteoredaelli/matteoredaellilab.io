---
title: 'Cambio programma'
date: Fri, 21 Nov 2008 08:26:10 +0000
draft: false
tags: ['Barzellette', 'Me']
---

Oggi mi sono svegliato presto, mi sono infilato i pantaloni, vestito lentamente, preparato caffè, preso le mie mazze da golf e sono andato piano verso il garage, ho messo le mazze nell'auto e ho tirato fuori la macchina dal garage sotto una pioggia torrenziale. La strada era totalmente inondata e il vento gelido soffiava a 100 km orari. Sono rientrato con l'auto in garage, acceso la radio e sentito che le previsioni dicevano che quel tempaccio sarebbe durato tutto il giorno. Che delusione! Sono rientrato in casa, mi sono rispogliato e silenziosamente sono scivolato a letto. Piano mi sono avvicinando a mia moglie stringendola e, mettendole una mano sulla natica, le ho sussurrato all'orecchio: "il tempo fuori è orribile" Lei mezza addormentata mi ha risposto: "Sì lo so, e tu pensa a quel cretino di mio marito che è andato a giocare a golf!"