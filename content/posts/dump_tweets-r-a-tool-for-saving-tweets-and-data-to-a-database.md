---
title: 'dump_tweets.R a tool for saving tweets and user data to a database'
date: Mon, 20 Jan 2014 19:57:38 +0000
draft: false
tags:
- R
- twitter
- database
---

[![](http://blogs.knoxnews.com/woodbery/assets_c/2013/01/twitter-logo-bird-thumb-275x251-26039.gif)](https://github.com/matteoredaelli/dump_tweets.R)   [dump\_tweets.R](https://github.com/matteoredaelli/dump_tweets.R) is a tool for  searching tweets and (recursively) crawl users from twitter: Data are then saved to a MySQL database and can finally be exported to .RData files dump\_tweets.R is sponsorized by [Associazione Rospo](http://associazionerospo.org)       Example:

Rscript search.R -q "#opensource"

2014-01-20 20:53:43 INFO::Connecting to TWITTER... 2014-01-20 20:53:43 INFO::Connecting to database=twitter, host=localhost with user=root 2014-01-20 20:53:43 INFO::using UTF8 code 2014-01-20 20:53:43 INFO::Searching for q=#opensource, sinceID=0 2014-01-20 20:53:57 INFO::Found 191 tweets 2014-01-20 20:53:57 INFO::maxID=425355265857187841 2014-01-20 20:53:57 INFO::Saving data to tweet table... 2014-01-20 20:53:58 INFO::saving data to search\_results table...
