---
title: LDAP search examples from command line
date: 2020-10-12
draft: false
tags:
- ldap
- command line
- active directory
---

## setup

In these examples I use kerberos authentication but you could also use basic authentication with -D and -w options

For semplicity I put common options into the environment and I have created an alias

	export LDAP_OPTS="-h myldap.group.example.com -LLL -Y GSSAPI -b dc=group,dc=example,dc=com  -o ldif-wrap=no "

	alias adsearch="/usr/bin/ldapsearch ${LDAP_OPTS}"

## Extract locked users

  adsearch "(&(objectCategory=Person)(objectClass=User)(lockoutTime>=1))" dn

## Expired passwords
   date -d "1601/1/1+$(expr 132469210596077795 / 10000000 )Seconds"

"(&(objectclass=user)(objectcategory=person)(!pwdlastset=0)(pwdlastset<={date})(!userAccountControl:1.2.840.113556.1.4.803:=65536))"

## Extracting the flat list of members of a group
How to recursively retreiving teh list of members of a group

"(&(objectclass=user)(memberOf:1.2.840.113556.1.4.1941:=CN=Qliksense_Professional,OU=Common Services,DC=redaelli,DC=org))"
