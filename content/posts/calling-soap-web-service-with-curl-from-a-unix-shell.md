---
title: 'Calling soap web services with curl from a unix shell'
date: Fri, 18 Jun 2010 07:41:17 +0000
draft: false
tags: ['LinkedIn', 'Me', 'OpenSource', 'shell', 'unix', 'WebService', 'xslt']
---

\[caption id="" align="alignleft" width="471" caption="Curl"\]![](http://curl.haxx.se/pix/curl-refined.jpg "curl")\[/caption\] How to call soap web services from a unix shell? it is easy with curl and, optionally, xsltproc ! Read the tutorial [soap requests with curl](http://docs.google.com/present/view?id=dfr68gz6_200dhctmwg4) !
---
### Comments:
#### 
[Alias Canterbury]( "MassAirUnits@gmail.com") - <time datetime="2010-07-02 00:36:42">Jul 5, 2010</time>

Thanks for posting this tutorial! I really appreciate it, considering that a lot of other websites posted vaguely similar instructions but I couldn't get them to work.
<hr />
#### 
[Test]( "Test@abc.com") - <time datetime="2010-12-02 12:38:20">Dec 4, 2010</time>

Thanks for such a nice post. Its a perfect working example. Its a good short guide to get started.
<hr />
