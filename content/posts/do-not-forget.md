---
title: 'Do not forget...'
date: Sun, 31 Jan 2010 07:39:57 +0000
draft: false
tags: ['Africa', 'Me', 'Volontariato']
---

Sometimes I need to watch this video, it helps me to not forget...

\[youtube\]http://www.youtube.com/watch?v=8hOaL6M4SDU\[/youtube\]

QUESTO SUCCEDE MENTRE NOI SIAMO ACCECATI DALL'AVIDITA'  E DAL CONSUMISMO...

E NOI FACCIAMO poco o NULLA PER CAMBIARE...

Questo video e' la mia bussola...