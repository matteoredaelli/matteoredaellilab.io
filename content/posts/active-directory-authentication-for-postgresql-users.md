---
title: Active Directory authentication for PostgreSQL users
date: 2020-10-12
draft: false
tags:
- postgres
- ldap
- active directory
---

It is easy, you just need to add to the configuration file /var/lib/postgresql/data/pg_hba.conf

```
host all all 0.0.0.0/0 ldap ldapserver="myldapserver" ldapbasedn="OU=USERS,DC=group,DC=redaelli,DC=org" ldapbinddn="CN=matteo,OU=USERS,DC=group,DC=redaelli,DC=org" ldapbindpasswd="MySillyPwd" ldapsearchattribute="sAMAccountName" ldapscheme="ldaps"
```

And inside your database yu need to create a role for the Active director users and then grant them to the required databases.
