---
title: 'Exporting database tables to csv files with Apache Camel'
date: Fri, 24 May 2019 07:12:48 +0000
draft: false
tags:
- apache
- camel
- database
---

Below the interested part of code using spring xml

```
 <bean id="ds-patriot-dw_ro" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
		 <property name="driverClassName" value="oracle.jdbc.OracleDriver" />
		  <property name="url" value="jdbc:oracle:thin:@//patriot.redaelli.org:1111/RED"/>
		  <property name="Username" value="user"/>
		  <property name="Password" value="pwd"/>
  </bean>


<camelContext id="MyCamel" streamCache="true" xmlns="http://camel.apache.org/schema/spring">

	<route id="scheduler">
	  <from uri="timer:hello?repeatCount=1"/>
	  <setHeader headerName="ndays">
		<constant>0</constant>
	  </setHeader>
	  <to uri="direct:start"/>
	</route>

	<route>
	  <from uri="direct:start"/>
	  <setBody>
		<constant>table1,table2,table3</constant>
	  </setBody>
	  <split streaming="true">
		<tokenize token="," />
		<setHeader headerName="tablename">
		  <simple>${body}</simple>
		</setHeader>
		<to uri="direct:jdbc2csv"/>
	  </split>
	</route>

	<route>
	  <from uri="direct:jdbc2csv"/>
		<to uri="direct:get-jdbc-data" pattern="InOut" />
		<to uri="direct:export-csv" />
	</route>

	<route>
	  <from uri="direct:get-jdbc-data"/>
	  <log message="quering table ${headers.tablename}..."/>

	  <setBody>
		<groovy><![CDATA[
		  "SELECT * from " + request.headers.get('tablename')
	]]>
		</groovy>
	  </setBody>
	  <log message="quering statement: ${body}..."/>
	  <to uri="jdbc:ds-patriot-dw_ro?useHeadersAsParameters=true&outputType=StreamList"/>
	</route>

	<route>
	  <from uri="direct:export-csv"/>
			<log message="saving table ${headers.tablename} to ${headers.CamelFileName}..."/>
	  <setHeader headerName="CamelFileName">
		<groovy>
		  request.headers.get('tablename').replace(".", "_") + "/" + request.headers.get('tablename') + ".csv"
		</groovy>
	  </setHeader>

	  <!-- <marshal><csv></marshal> does not include header. I have to export it manualy.. -->

	  <multicast stopOnException="true">
		<pipeline>
		  <log message="saving table ${headers.tablename} header to ${headers.CamelFileName}..."/>
		  <setBody>
	<groovy>request.headers.get('CamelJdbcColumnNames').join(";") + "\n"</groovy>
		  </setBody>
		  <to uri="file:output"/>
		</pipeline>

		<pipeline>
		  <log message="saving table ${headers.tablename} rows to ${headers.CamelFileName}..."/>
		  <marshal>
			<csv delimiter=";" headerDisabled="false" useMaps="true"/>
		  </marshal>
		  <to uri="file:output?fileExist=Append"/>
		</pipeline>
	  </multicast>

	  <log message="saved table ${headers.tablename} to ${headers.CamelFileName}..."/>
  </route>

  </camelContext>
```
