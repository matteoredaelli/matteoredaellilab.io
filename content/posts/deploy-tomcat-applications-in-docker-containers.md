---
title: 'Deploy tomcat applications in Docker containers'
date: Tue, 14 Mar 2017 16:37:34 +0000
draft: false
tags:
- docker
- tomcat
- java
---

![](http://tomcat.apache.org/images/tomcat.png)Deploying you applications in containers, you are sure that they are easily portable and scalable... Here a sample of deploying a .war application using a Docker container Create a Dockerfile like```
FROM tomcat:8-jre8

MAINTAINER "Matteo <matteo.redaelli@gmail.com>"

ADD server.xml /usr/local/tomcat/conf/
ADD tomcat-users.xml /usr/local/tomcat/conf/
ADD ojdbc6.jar /usr/local/tomcat/lib/
ADD bips.war /usr/local/tomcat/webapps/
```Build a docker image```
docker build . -t myapp
```Run one or more docker images of your appplication with```
docker run --restart=unless-stopped --name myapp1 -p 8080:8080 -d myapp
docker run --restart=unless-stopped --name myapp2 -p 8081:8080 -d myapp
```It is better to redirect tomcat logs to stdout: in this way you can see them with```
docker logs myapp
```Docker containers can be managed among several servers using tools like [Kubernetes (](https://kubernetes.io/)an open-source system for automating deployment, scaling, and management of containerized applications), but it should be an other post ;-)
