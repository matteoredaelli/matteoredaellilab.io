---
title: 'Oracle Opensource NoSQL Hadoop R'
date: Sat, 08 Oct 2011 10:01:56 +0000
draft: false
tags: ['Oracle', 'OpenSource', R]
---

**Oracle's comprehensive big data strategy includes NoSQL, Hadoop, and R analytics** "Oracle's planned distribution of the open-source R statistical environment will be adapted for use on large-scale data within the Oracle database, rather than on desktops and laptops where analysts typically use the software. Oracle R Enterprise will run existing R applications and it will use the R client directly against data stored in Oracle Database 11g. This will vastly increase scalability, performance, and security, according to Oracle, along with the promise of software support. Oracle will ship the open-source distribution along with Linux. Separate R packages with database-specific extensions for Oracle 11g will be bundled with that database". Taken from an [Informationweek article](http://www.informationweek.com/news/software/info_management/231700205).
