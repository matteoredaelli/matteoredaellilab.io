---
title: 'Foto della ciaspolata a S. Caterina con gli Amici della Montagna'
date: Mon, 04 Feb 2008 12:09:54 +0000
draft: false
tags: ['Me', 'Me']
---

In questa pagina trovate le [foto](http://www.redaelli.org/pictures/2008/) della ciaspolata di ieri a S. Caterina organizzata dagli Amici della Montagna ([Alpini di Carate Brianza](http://www.alpinicarate.it))