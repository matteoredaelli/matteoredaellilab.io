---
title: Setting up a workstation/server and managing dotfiles centrally using Ansible and GNU Stow
date: 2024-09-09
draft: false
tags:
- ansible
- programming
- stow
- dotfiles
---

## About

In the world of software development, maintaining a consistent and efficient development environment across multiple machines can be challenging. This blog post explores how to leverage two powerful tools, Ansible and GNU Stow, to automate the setup of your development environment and manage your dotfiles effectively.

## The Power of Automation with Ansible

Ansible is an open-source automation tool that excels in configuration management, application deployment, and task automation. Its key advantages include:

1. **Agentless architecture**: Ansible doesn't require any agents to be installed on managed nodes, making it lightweight and easy to set up.
2. **YAML-based playbooks**: Ansible uses human-readable YAML files to describe automation jobs, making it easy to write and understand.
3. **Idempotency**: Ansible ensures that running the same playbook multiple times produces the same result, making it safe to re-run automation scripts.
4. **Extensibility**: With a vast collection of modules, Ansible can automate almost any IT task.

## Simplifying Dotfile Management with GNU Stow

GNU Stow is a symlink farm manager that takes a different approach to managing dotfiles:

1. **Symlink-based**: Stow creates symlinks from your home directory to your actual dotfiles, keeping them organized in a separate directory.
2. **Package-oriented**: It allows you to group related dotfiles into "packages," making it easy to manage configurations for different tools.
3. **Non-intrusive**: Stow doesn't overwrite existing files, ensuring the safety of your current configurations.
4. **Version control friendly**: By keeping your dotfiles in a separate directory, it's easy to version control them with Git or other VCS tools.

## Putting It All Together

```bash
mkdir $HOME/ansible_dotfile # should be in the $HOME
mkdir $HOME/ansible_dotfile/dotfiles
mkdir $HOME/ansible_dotfile/dotfiles_work
```

Now put your configuration files (like .bash_profile, .config/emacs/init.el, .ssh/config in the "dofiles" subfolder (like $HOME/ansible_dotfile/dotfiles/.config/emacs/init.el)

Let's look at how we can combine Ansible and GNU Stow to set up a development environment and manage dotfiles. Here's a breakdown of the sample Ansible playbook bootstrap.yml:

```yaml
## Run with ansible-playbook bootstrap.yml
- name: Bootstrap environment for Matteo laptop
  hosts: localhost
  tasks:
    - name: Update homebrew and upgrade all packages
      community.general.homebrew:
        update_homebrew: true
        upgrade_all: true

    - name: Install packages with brew
      community.general.homebrew:
        name:
          - awscli
          - dbeaver-community
          - devtunnel
          - emacs
          - firefox
          - google-chrome
        state: present
      when: ansible_distribution == "MacOSX"
      
    - name: Install languages packages with brew
      community.general.homebrew:
        name:
          - clojure
          - erlang
          - jq
          - python@3.12
          - swi-prolog
        state: present
      when: ansible_distribution == "MacOSX"

    - name: Run stow
      shell: "stow dotfiles  --verbose=2 --no-folding -R"
      register: result
      changed_when: 'result.stderr is search("LINK: ")'  

    - name: Run stow for my job config files
      shell: "stow dotfiles_work  --verbose=2 --no-folding -R"
      register: result
      changed_when: 'result.stderr is search("LINK: ")'

    - name: Steampipe plugins
      shell: "steampipe plugin install jira whois aws ldap net rss"
      register: result
      changed_when: 'result.stderr is search("Installed plugin")'

    - name: Steampipe plugins
      shell: "steampipe plugin update --all"
      register: result
```

This playbook does the following:

1. Updates Homebrew and all installed packages.
2. Installs a set of common development tools and applications using Homebrew.
3. Installs programming languages and related tools.
4. Uses GNU Stow to manage dotfiles for both personal and work environments.
5. Installs and updates Steampipe plugins for various services.

## Benefits of This Approach

1. **Reproducibility**: Your entire development environment can be recreated with a single command.
2. **Version Control**: Both your Ansible playbook and dotfiles can be version-controlled, allowing for easy tracking of changes and rollbacks if needed.
3. **Flexibility**: The playbook can be easily modified to add or remove packages as your needs change.
4. **Cross-platform**: With proper conditionals, you can use the same playbook across different operating systems.
5. **Separation of Concerns**: Personal and work-related dotfiles are managed separately, allowing for easy context switching.

## Conclusion

By combining Ansible's powerful automation capabilities with GNU Stow's elegant dotfile management, you can create a robust, version-controlled, and easily reproducible development environment. This approach not only saves time in setting up new machines but also ensures consistency across all your development environments.

Remember to keep your Ansible playbook and dotfiles in a version control system like Git, and consider hosting them on a platform like GitHub for easy access across different machines. Happy coding!
```

This blog post provides an overview of using Ansible and GNU Stow for development environment setup and dotfile management, explains the advantages of both tools, and breaks down the sample configuration file you provided. It should give readers a good understanding of how to implement this approach in their own workflows.
