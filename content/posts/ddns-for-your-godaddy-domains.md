---
title: 'DDNS for your GoDaddy domains'
date: Wed, 13 Apr 2016 20:04:52 +0000
draft: false
tags: ['dns']
---

You can automatically update DNS for your GoDaddy.com domains following the suggestions from the post [ddns-with-godaddy](http://blogs.umb.edu/michaelbazzinott001/2014/09/14/ddns-with-godaddy/) but remember to patch the godaddy python library with [this client.py](https://github.com/claneys/pygodaddy/blob/master/pygodaddy/client.py)
