---
title: 'Twitter data mining with R: generating wordclouds'
date: Mon, 01 Aug 2011 20:06:48 +0000
draft: false
tags: ['data mining', 'R', 'twitter', 'wordcloud']
---

[![](http://www.redaelli.org/matteo-blog/wp-content/uploads/2011/08/linux.png "linux")](http://www.redaelli.org/matteo-blog/wp-content/uploads/2011/08/linux.png)This image is a sample **Word cloud** about **tweets** related to the word **linux**. It was generated with **R** (see source code [twitter-r-utils](https://github.com/matteoredaelli/twitter-r-utils)) following the examples from:

*   [text data ming with Twitter and R](http://heuristically.wordpress.com/2011/04/08/text-data-mining-twitter-r/)
*   [wordcloud in R](http://onertipaday.blogspot.com/2011/07/word-cloud-in-r.html)
