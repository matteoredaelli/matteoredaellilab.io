---
title: 'Using Apache Camel with Ibatis'
date: Wed, 08 Oct 2008 20:54:26 +0000
draft: false
tags: ['Apache', ' Camel', 'OpenSource']
---

This is a quick tutorial about the usage of the very powerful Integration tool [Apache Camel](http://activemq.apache.org/camel) with [Apache Ibatis](ibatis.apache.org/). Many thanks to Apache Camel forums and [Claus Ibsen](http://www.nabble.com/user/UserProfile.jtp?user=1189612).

REQUIREMENTS
============

*   I used Java version 1.5.0\_16
*   [Apache Maven](http://maven.apache.org/) version 2.0.9
*   A database mysql: I used a local db "moodle", user "moodle", password "moodle" and a table "mdl\_user" \[ create table mdl\_user ( id int, firstname varchar(30), lastname varchar(30), email varchar(30) ); \]

DOWNLOAD
========

The full sample project can be download from [here](http://www.redaelli.org/matteo/downloads/apache-camel/demo-ibatis.tgz)!

STEP BY STEP
============

As suggested by http://activemq.apache.org/camel/creating-a-new-spring-based-camel-route.html matteo@nowar:~/workspace$ mvn archetype:create \\ -DarchetypeGroupId=org.apache.camel \\ -DarchetypeArtifactId=camel-router \\ -DarchetypeVersion=1.4.0 \\ -DgroupId=org.redaelli \\ -DartifactId=demo-ibatis matteo@nowar:~/workspace$ cd demo-ibatis matteo@nowar:~/workspace$ mvn eclipse:eclipse matteo@nowar:~/workspace$ mvn install Create iBatis files

*   src/main/resources/SqlMapConfig.xml
*   src/main/resources/User.xml
*   src/main/java/org/redaelli/User.java

Add the routes and dependences in the files

*   src/main/java/org/redaelli/MyRouteBuilder.java
*   pom.xml (adding dependencies "camel-ibatis" e "mysql jdbc")

And finally run the demo with matteo@nowar:~/workspace$ mvn camel:run
