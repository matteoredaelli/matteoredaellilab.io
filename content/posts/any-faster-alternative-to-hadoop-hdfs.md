---
title: 'Any faster alternative to #Hadoop HDFS?'
date: Thu, 17 Nov 2016 08:36:05 +0000
draft: false
tags:
- hadoop
- apache
- opensource
- s3
- chep
- lustre
- file system
---

I'd like to have an alternative to Hadoop HDFS, a faster and not java filesystem:

*   [S3:](https://aws.amazon.com/it/s3/) [S3 Support in Apache Hadoop](https://wiki.apache.org/hadoop/AmazonS3) if your servers are hosted at Amazon AWS
*   [chep](http://ceph.com/): using [hadoop with ceph](http://docs.ceph.com/docs/jewel/cephfs/hadoop/)
*   [glusterfs](https://www.gluster.org/): [managing hadoop compatible storage](https://gluster.readthedocs.io/en/latest/Administrator%20Guide/Hadoop/)
*   [lustre](http://www.lustre.org/): [Running hadoop with lustre](http://wiki.lustre.org/index.php/Running_Hadoop_with_Lustre)
*   [Openstack Swift](http://docs.openstack.org/developer/swift/): [Hadoop OpenStack Support: Swift Object Store](https://hadoop.apache.org/docs/stable2/hadoop-openstack/index.html#Hadoop_OpenStack_Support:_Swift_Object_Store)
*   [xstreamfs](http://www.xtreemfs.org): there is an [hadoop client](http://www.xtreemfs.org/downloads/XtreemFSHadoopClient.jar)

Which is better? Any suggestions? References:

*   \[1\] https://en.wikipedia.org/wiki/Comparison\_of\_distributed\_file\_systems
