---
title: 'Sentiment analysis of Tyre competitors from Twitter and R'
date: Sun, 12 Feb 2012 17:00:19 +0000
draft: false
tags: ['tyre', 'twitter']
---

Below a simple example of sentiment analyis of tweets about tyre competitors. Notes:

*   x-axes=positive tweets
*   y-axes=negative tweets
*   positive and negative words: Hu and Liu’s “opinion lexicon” categorizes nearly 6,800 words as positive or negative and were downloaded from Bing Liu’s web site: http://www.cs.uic.edu/~liub/FBS/opinion-lexicon-English.rar
*   only more positive and worst negative tweets are collected

[![](https://github.com/matteoredaelli/twitter-r-utils/raw/master/output/scatter.png "Tyre competitors")](https://github.com/matteoredaelli/twitter-r-utils)[![](https://github.com/matteoredaelli/twitter-r-utils/raw/master/output/score.png "tyre competitors")](https://github.com/matteoredaelli/twitter-r-utils/) The code can be found [here](https://github.com/matteoredaelli/twitter-r-utils).
