---
title: 'Cena di beneficenza'
date: Thu, 06 Sep 2007 14:41:38 +0000
draft: false
tags: ['Uncategorized', 'Volontariato']
---

Vi propongo la cena di beneficenza _sabato 13 ottobre 2007_  alle _ore 20_ presso l’Agriturismo “I LAGHETTI” via Cavamarna (Oasi di Baggero) - Monguzzo (CO) durante la quale verra' presentato il gruppo di volontariato **Binario per l'Africa**. Per maggiori informazioni andate sul sito [http://binarioperlafrica.wordpress.com/](http://binarioperlafrica.wordpress.com/) e se volete ad agosto andare in Kenia nella zona del Meru per vedere con i vostri occhi a cosa servono i contributi raccolti...