---
title: '#Cascading will support Apache #Tez, #Apache #Spark and Apache #Storm'
date: Mon, 19 May 2014 19:51:27 +0000
draft: false
tags:
- apache
- hadoop
---

![](http://www.cascading.org/wp-content/themes/element-child/images/logo-projects.png) In these nights I'm playing with [Cascalog](http://cascalog.org/). for running ([map reduce](http://wiki.apache.org/hadoop/MapReduce)) jobs over [Hadoop](http://hadoop.apache.org/). A nice news for the future is that "[Cascading](http://www.cascading.org) 3.0 will initially ship with support for: local in-memory, Apache MapReduce (support for both [Hadoop](http://hadoop.apache.org/) 1 and 2 are provided), and [Apache Tez](http://www.redaelli.org/matteo-blog/2014/03/12/apache-tez-for-hadoop-2-0/). Soon thereafter, with community support, [Apache Spark](http://spark.apache.org/)™, [Apache Storm](http://storm.incubator.apache.org/) and others will be supported through its new pluggable and customizable planner\[...\]". Read the full [InfoQ article](http://www.infoq.com/news/2014/05/driven)
