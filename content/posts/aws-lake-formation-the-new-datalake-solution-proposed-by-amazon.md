---
title: 'AWS Lake Formation: the new Datalake solution proposed by Amazon'
date: Thu, 29 Nov 2018 21:04:11 +0000
draft: false
tags:
- aws
- data lake
- bigdata
---

[AWS Lake Formation](https://aws.amazon.com/lake-formation/) is a service that makes it easy to set up a secure data lake in days. A data lake is a centralized, curated, and secured repository that stores all your data, both in its original form and prepared for analysis. A data lake enables you to break down data silos and combine different types of analytics to gain insights and guide better business decisions.

However, setting up and managing data lakes today involves a lot of manual, complicated, and time-consuming tasks. This work includes loading data from diverse sources, monitoring those data flows, setting up partitions, turning on encryption and managing keys, defining transformation jobs and monitoring their operation, re-organizing data into a columnar format, configuring access control settings, deduplicating redundant data, matching linked records, granting access to data sets, and auditing access over time.

Creating a data lake with Lake Formation is as simple as defining where your data resides and what data access and security policies you want to apply. Lake Formation then collects and catalogs data from databases and object storage, moves the data into your new Amazon S3 data lake, cleans and classifies data using machine learning algorithms, and secures access to your sensitive data. Your users can then access a centralized catalog of data which describes available data sets and their appropriate usage. Your users then leverage these data sets with their choice of analytics and machine learning services, like Amazon EMR for Apache Spark, Amazon Redshift, Amazon Athena, Amazon Sagemaker, and Amazon QuickSight. \[[aws.amazon.com](https://aws.amazon.com/lake-formation/)\]

Lake Formation automatically configures underlying AWS services, including S3, AWS Glue, AWS IAM, AWS KMS, Amazon Athena, Amazon Redshift, and Amazon EMR for Apache Spark, to ensure compliance with your defined policies. If you’ve set up transformation jobs spanning AWS services, Lake Formation configures the flows, centralizes their orchestration, and lets you monitor the execution of your jobs. With Lake Formation, you can configure and manage your data lake without manually integrating multiple underlying AWS services

Sources:

*   [https://aws.amazon.com/lake-formation/features/](https://aws.amazon.com/lake-formation/features/)
*   [https://www.zdnet.com/article/aws-unveils-lake-formation-for-easy-data-lake-building/](https://www.zdnet.com/article/aws-unveils-lake-formation-for-easy-data-lake-building/)
