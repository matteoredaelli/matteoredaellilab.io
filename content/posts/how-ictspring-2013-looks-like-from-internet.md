---
title: 'How #ictspring 2013 looks like from Internet'
date: Wed, 19 Jun 2013 20:56:06 +0000
draft: false
tags: ['Me']
---

Looking at tweets of day 1 (2013-06-19) of #[ictspring 2013](http://www.ictspring.com/) I see

[![who-retweets-whom2](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/who-retweets-whom2.png)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/who-retweets-whom2.png)

TOP CONTRIBUTORS[](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/who-retweets-whom2.png)[![top-contributors](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/top-contributors.png)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/top-contributors.png)

MOST USED DEVICES

[![agents](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/agents.png)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/agents.png)

GIVEN TOPICS

[![given-topics](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/given-topics.png)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/given-topics.png)

TWEETS DURING THE DAY

[![tot-tweets](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/tot-tweets.png)](http://www.redaelli.org/matteo-blog/wp-content/uploads/2013/06/tot-tweets.png)