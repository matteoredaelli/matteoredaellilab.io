---
title: 'About Cayley a scalable graph database'
date: Mon, 03 Aug 2015 13:17:24 +0000
draft: false
tags:
- opensource
- cayley
- database
- graph
---

[![](https://raw.githubusercontent.com/google/cayley/master/static/branding/cayley_side.png)](https://github.com/google/cayley)   This is fast tutorial of using the [Caylay graph database](https://github.com/google/cayley) (with [MongoDB](https://www.mongodb.org/) as backend): Cayley is "not a Google project, but created and maintained by a Googler, with permission from and assignment to Google, under the Apache License, version 2.0"

*   download and unzip a [binary distribution](https://github.com/google/cayley/releases)
*   edit cayley.cfg

```
{
"database": "mongo",
"db\_path": "cayley.redaelli.org:27017",
"read\_only": false,
"host": "0.0.0.0"
}
```

*   ./cayley init -config=cayley.cfg
*   ./cayley http -config=cayley.cfg -host="0.0.0.0" &
*   create a file demo.n3

```
"/user/matteo" "is\_manager\_of" "/user/ele" .
"/user/matteo" "has" "/workstation/wk0002" .
"/user/matteo" "lives\_in" "/country/italy" .
```

*   upload data with: curl http://cayley.redaelli.org:64210/api/v1/write/file/nquad -F NQuadFile=@demo.n3
*   or: ./cayley load --config=cayley.cfg  -quads=demo.n3
*   query data with: curl --data 'g.V("/user/matteo").Out(null,"predicate").All()' http://cayley.redaelli.org:64210/api/v1/query/gremlin

```
{
 "result": \[
  {
   "id": "/workstation/wk0002",
   "predicate": "has"
  },
  {
   "id": "/country/italy",
   "predicate": "lives\_in"
  },
  {
   "id": "/user/ele",
   "predicate": "is\_manager\_of"
  }
 \]
```
