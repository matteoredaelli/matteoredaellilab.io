---
title: 'Top twitter users statistics'
date: Tue, 13 May 2014 19:36:09 +0000
draft: false
tags: ['twitter']
---

Starting collecting statistics about top (suggested) twitter users at site [http://top-twitter-users.blogspot.it/](http://top-twitter-users.blogspot.it/)

*   [sport: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/sport-statistiche-aprile-2014.html)
*   [satira: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/satira-statistiche-aprile-2014.html)
*   [parlano-di-twitter: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/parlano-di-twitter-statistiche-aprile.html)
*   [club-della-serie-a-tim: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/club-della-serie-tim-statistiche-aprile.html)
*   [calciatori: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/calciatori-statistiche-aprile-2014.html)
*   [scienza-tecnologia: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/scienza-tecnologia-statistiche-aprile.html)
*   [programmi-tv: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/programmi-tv-statistiche-aprile-2014.html)
*   [spettacolo: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/spettacolo-statistiche-aprile-2014.html)
*   [arte-moda-design: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/arte-moda-design-statistiche-aprile-2014.html)
*   [governo-istituzioni: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/governo-istituzioni-statistiche-aprile.html)
*   [musica: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/musica-statistiche-aprile-2014.html)
*   [notizie: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/notizie-statistiche-aprile-2014.html)
*   [non-profit: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/non-profit-statistiche-aprile-2014.html)
*   [politica: statistiche aprile 2014](http://top-twitter-users.blogspot.it/2014/05/politica-statistiche-aprile-2014.html)
