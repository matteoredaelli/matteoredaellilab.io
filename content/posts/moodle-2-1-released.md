---
title: 'Moodle 2.1 released!'
date: Thu, 07 Jul 2011 19:26:08 +0000
draft: false
tags: ['elearning', 'Linux', 'Moodle', 'mysql', 'OpenSource', 'php']
---

![](http://moodle.org/pluginfile.php/51/mod_forum/post/785432/moodle-2.1.jpg "Moodle") "[Moodle](http://www.moodle.org) is a software package for producing Internet-based courses and web sites. It is a global development project designed to support a social constructionist framework of education. Moodle is provided freely as Open Source software (under the GNU Public License)." \[from [About\_Moodle](http://docs.moodle.org/20/en/About_Moodle)\] "You can read more on the [Moodle 2.1 release notes](http://docs.moodle.org/dev/Moodle_2.1_release_notes), but the major new features are:

*   A whole new question engine (used by quizzes, for example), which makes questions more robust and will enable developers to create all kinds of interesting question types in the future. Some interesting new features like Certainty-Based Marking have also been added. Huge thanks are due to Tim Hunt and Open University for all their work on this.
*   Restoring of Moodle 1.9 backups is now possible! Thanks to Moodlerooms for their help in some of the key parts of the core work and to the HQ team for the rest. This was a very complex thing to build due to the many changes between 1.9 and 2.x. Note that only course content in core modules is restored: if you use other activity modules then you need to ask (or help) the developers of those modules to add the new module restore code (thanks to the design this is not a huge task to implement).
*   Moodle 2.1 now directly supports mobile apps for some functions. The new Moodle app for iPhone (by Moodle HQ) will be released soon (followed by an Android version later) and these require the secure web services of Moodle 2.1 or later. In addition, there is direct support for mobile-specific Moodle Themes and automatic detection of mobile browsers.
*   [Moodle 2.0 Release notes](http://docs.moodle.org/dev/Moodle_2.0_release_notes)

  There are many other smaller improvements too, but one of the ones I really like is the new and improved configuration interface for blocks. You should find it a lot easier to understand than the one in Moodle 2.0." \[from [Moodle News](http://moodle.org/news/)\]
