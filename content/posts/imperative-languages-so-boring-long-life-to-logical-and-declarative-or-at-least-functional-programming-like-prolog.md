---
title: 'Imperative (not functional) languages are so boring. Long life to logical and declarative programming like Prolog'
date: Sat, 18 Apr 2020 19:21:00 +0000
draft: false
tags:
- prolog
- programming
- opensource
---

Below a simple example of using a a declarative language ([Prolog](https://en.wikipedia.org/wiki/Prolog)) for finding all solutions for a trick game...

```
?- use_module(library(clpfd)).
?- X in 1..9, Y #= (X * 3 + 3) * 3, Z #= (Y rem 10) + (Y div 10), setof(Z, X^Y^label([X,Y,Z]), Sol).

Sol = [9],
```

![](http://www.redaelli.org/matteo-blog/wp-content/uploads/2020/04/prolog-clp.jpg)
